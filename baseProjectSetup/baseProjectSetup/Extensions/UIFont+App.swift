//
//  UIFont+App.swift
//  PriortySMS
//
//  Created by Dan Hillman on 29/11/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    static let extraSmallFontSize = CGFloat(9.0)
    static let smallFontSize = CGFloat(11.0)
    static let mediumSmallFontSize = CGFloat(13.0)
    static let mediumFontSize = CGFloat(14.0)
    static let mediumLargeFontSize = CGFloat(16.0)
    static let largeFontSize = CGFloat(18.0)
    static let large2FontSize = CGFloat(23.0)
    static let navBarTitleSize = CGFloat(20.0)
    static let largeXLFontSize = CGFloat(30.0)
    
    
    // EXAMPLE FONT AND HOW TO ADD
    /**********************************************************************************
     * Avenir Next
     **********************************************************************************/
    
    //    ["AvenirNext-DemiBold"]
    
//    class func app_AvenirNextDemiBold(size:CGFloat) -> UIFont {
//        return UIFont(name: "AvenirNext-DemiBold", size: size)!
//    }

    
//    //AVENIR NEXT DEMI BOLD
//
//    class func app_avenirNextDemiBoldLargeStratfordRC() -> UIFont {
//        return  app_AvenirNextDemiBold(size: largeFontSize)
//    }
//
//    class func app_avenirNextDemiBoldMediumStratfordRC() -> UIFont {
//        return  app_AvenirNextDemiBold(size: mediumFontSize)
//    }
//
//    class func app_avenirNextDemiBoldMediumSmallStratfordRC() -> UIFont {
//        return  app_AvenirNextDemiBold(size: mediumSmallFontSize)
//    }
//
//    class func app_avenirNextDemiBoldSmallStratfordRC() -> UIFont {
//        return  app_AvenirNextDemiBold(size: smallFontSize)
    }


