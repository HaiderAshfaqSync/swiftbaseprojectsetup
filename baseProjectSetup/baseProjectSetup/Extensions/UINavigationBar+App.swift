//
//  UINavigationBar+App.swift
//  PriortySMS
//
//  Created by Haider Ashfaq on 31/08/2017.
//  Copyright © 2017 Sync Interactive. All rights reserved.
//

import Foundation

extension UINavigationBar
{
    
    func setCenterNavBarLogo() {
    
        let imageView: UIImageView = {
            let imageView = AppUIBuilder.getImageView(UIImage(named: "nav-bar-center-logo"),
                                                      contentMode: .scaleAspectFill)
            imageView.clipsToBounds = true
            imageView.isUserInteractionEnabled = false
            imageView.tag = 999
            return imageView
        }()
        
        if !imageView.isDescendant(of: self) {
            self.addSubview(imageView)
            self.clipsToBounds = false
            imageView.autoCenterInSuperview()
        } else {
            print("I EXIST")
            imageView.removeFromSuperview()
        }
    }
    
    func removeCenterNavBarLogo() {
        for view in self.subviews {
            if (view.tag == 999) {
                print("MATCH 999!!!")
                view.removeFromSuperview()
            }
        }
    }
    
    func styleGradientNavBar() {
        var colors = [UIColor]()
        colors.append(UIColor.red),
        colors.append(UIColor.blue)
        self.apply(gradient: colors)
    }
    
    /// Applies a background gradient with the given colors
    func apply(gradient colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += SI.SCREEN_SIZE.STATUS_BAR_HEIGHT
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    /// Creates a gradient image with the given settings
    static func gradient(size : CGSize, colors : [UIColor]) -> UIImage?
    {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: 0.0, y: size.height), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
