//
//  UIColor+App.swift
//  PriortySMS
//
//  Created by Dan Hillman on 29/11/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    // Mark: - Standard colors
    
    class func app_hud() -> UIColor {
        return UIColor(0, 75, 134, 1.0) // Blue
    }

    // MARK: - Social Colours
    
    class func app_faceBook() -> UIColor {
        if AppTarget.isTargetSheffieldDogs() {
            return UIColor(72, 115, 201, 1.0)
        }
        
        return UIColor.colorWithHTMLRGB(0x3b5998)
    }
    
    class func app_twitter() -> UIColor {
        return UIColor.colorWithHTMLRGB(0x0084b4)
    }
    
    class func app_instagram() -> UIColor {
        return UIColor.colorWithHTMLRGB(0xcd486b)
    }
    
}


