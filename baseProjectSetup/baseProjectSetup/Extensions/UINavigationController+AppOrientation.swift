//
//  UINavigationController+App.swift
//  PriortySMS
//
//  Created by Dan Hillman on 11/10/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    // MARK: Orientation
    
    open override var shouldAutorotate : Bool {
        if let visibleViewController = viewControllers.last {
            return visibleViewController.shouldAutorotate
        }
        
        return true
    }
    
    open override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if let visibleViewController = viewControllers.last {
            return visibleViewController.supportedInterfaceOrientations
        }
        
        return .portrait
    }
    
    open override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        if let visibleViewController = viewControllers.last {
            return visibleViewController.preferredInterfaceOrientationForPresentation
        }
        
        return .portrait
    }
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        if let visibleViewController = viewControllers.last {
            return visibleViewController.preferredStatusBarStyle
        }
        return .lightContent
    }
    
}
