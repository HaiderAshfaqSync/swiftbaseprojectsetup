//
//  SILabel+App.swift
//  PriortySMS
//
//  Created by Haider Ashfaq on 23/08/2017.
//  Copyright © 2017 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension SILabel {
    
    //MARK: - App Text Alert (Title and body labels)
    
    class func label_AppTextAlertTitle() -> SILabel {
        var label: SILabel!
        if AppTarget.isTargetSheffieldDogs() || AppTarget.isTargetBarnsleyFC() || AppTarget.isTargetBristolBears() {
            label = AppUIBuilder.getLabel(
                nil,
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_HindGunturBoldFontSizeLarge(),
                textAlignment: .center
            )
        } else if AppTarget.isTargetBristolBears() {
            label = AppUIBuilder.getLabel(
                nil,
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_VanguardBoldFontSizeLarge(),
                textAlignment: .center
            )
        } else {
            label = AppUIBuilder.getLabel(
                nil,
                textColor: UIColor.BristolCityFC_ButtonColor(),
                numberOfLines: 0,
                font: UIFont.app_BebasNeueFontSizeLarge(),
                textAlignment: .center
            )
        }
        
        return label
    }
    
    class func label_AppTextAlertBody() -> SILabel {
        var label: SILabel!
        
        if AppTarget.isTargetBristolBears() {
            label = AppUIBuilder.getLabel(
                nil,
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_InterstateRegularFontSizeMediumSmall(),
                textAlignment: .center
            )
        } else {
         label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_HindGunturRegularFontSizeMediumSmall(),
            textAlignment: .center
        )
        }
        return label
    }
    
     //MARK: - Fixtures and Results / Match Centre
    
    // Match Centre Header Time/Score
    class func label_MatchCentreFixturesResultsTimeScoreCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.teamPrimaryColorFaded(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturSemiBold(size: 30.0), //change for phone sizes TO DO:
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    // Fixtures/Results Time/Score
    class func label_FixturesResultsTimeScoreCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.teamPrimaryColorFaded(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturSemiBoldFontSizeLarge(), //change for phone sizes TO DO:
            textAlignment: .center 
        )
        label.textPosition = .default
        return label
    }
    
    class func label_FixturesTimeScoreCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.teamPrimaryColorFaded(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturSemiBold(size: 18.0), //change for phone sizes TO DO:
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreFixturesResultsTimeScoreCellHT() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.teamPrimaryColorFaded(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturSemiBold(size: 25.0), //change for phone sizes TO DO:
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    // Team Titles
    class func label_MatchCentreTeamTitleCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_HindGunturSemiBoldFontSizeMedium(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreFixtureResultsTeamTitleCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: UIFont.app_HindGunturSemiBoldFontSizeMedium(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    //FT/HT labels
    class func label_MatchCentreFixtureResultsFTLabelCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_HindGunturSemiBoldFontSizeLarge(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreFixtureResultsHTLabelCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_HindGunturSemiBoldFontSizeMedium(),
            textAlignment: .left
        )
        label.textPosition = .bottom
        return label
    }
    
    class func label_MatchCentreFixturesResultsChampionshipCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: UIFont.app_HindGunturMediumFontSizeLarge(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreHeaderChampionshipCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.teamPrimaryColor(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturMediumFontSizeSmall(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreFixturesResultsDateCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.BristolCityFCWarmGrey(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturRegularFontSizeLarge(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreDateCell() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_HindGunturRegularFontSizeMediumSmall(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    //MARK: - MatchCentre TableView Headers
    class func label_MatchCentreCountdownHeaderLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: UIFont.app_HindGunturMediumFontSizeMedium(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreStarContestTitleHeaderLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.starContestActiveCompetitionCellTitleColor(),
            numberOfLines: 0,
            font: UIFont.app_avenirNextMediumFontSmallStratfordRC(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    // Match centre screen star contest preview for winners
    class func label_MatchCentreStarContestWinnersNameLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.starContestActiveCompetitionCellTitleColor(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturMediumFontSizeMedium(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreStarContestWinnersVoteNumLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.starContestActiveCompetitionCellTitleColor(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturBoldFontSizeMedium(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    // Star contest full result vc screen
    class func label_MatchCentreStarContestFullResultNameLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: UIFont.app_HindGunturMediumFontSizeMedium(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreStarContestFullResultVoteNumLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: UIFont.app_HindGunturBoldFontSizeMedium(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    //MARK: - MatchCentre OverView Match Information labels
    class func label_MatchCentreOverviewMatchInfoLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.BristolCityFCWarmGrey(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturRegularFontSizeMedium(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreOverviewMatchInfoTimeLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: UIFont.app_HindGunturBoldFontSizeMedium(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreOverviewMatchInfoPlayerLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.keyEventsPlayerNameColor(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturRegularFontSizeMedium(),
            textAlignment: .center
        )
        label.textPosition = .default
        return label
    }
    
    //Match Centre Commentary Cell
    
    class func label_MatchCentreCommentaryLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: UIFont.app_HindGunturMediumFontSizeSmall(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    //Match Centre Form Cell for Rugby Apps
    
    class func label_MatchCentreFormTitleLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.app_blackVarient(),
            numberOfLines: 0,
            font: UIFont.app_InterstateBoldFontSizeNavBarTitle(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreFormDateVenueLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.app_silverish(),
            numberOfLines: 0,
            font: UIFont.app_InterstateRegularFontSizeMedium(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreFormHomeAwayLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.app_silverish(),
            numberOfLines: 0,
            font: UIFont.app_InterstateBoldFontSizeMedium(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    class func label_MatchCentreFormResulIndicatorLabel() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_InterstateRegularFontSizeLarge(),
            textAlignment: .left
        )
        label.textPosition = .default
        return label
    }
    
    
    //MARK: - Competitions VC Header Title/Description labels
    
    class func label_AppCompetitionHeaderTitle() -> SILabel { //colors are overridden in cell class
        
        var font: UIFont!
        
        if AppTarget.isTargetBristolBears() {
            font = UIFont.app_VanguardBold(size: 30)
        } else {
            font = UIFont.app_HindGunturBold(size: 30)
        }
        
        let label = AppUIBuilder.getLabel(
            Localised.key("competitions").uppercased(),
            textColor: UIColor.mainCompetitionHeaderTitleColor(),
            numberOfLines: 0,
            font: font,
            textAlignment: .left
        )
        label.textPosition = .default
        //                label.debugView()
        return label
    }
    
    //MARK: - Competitions Tableview Cells
    
    class func label_AppCompetitionCellTitle() -> SILabel { //colors are overridden in cell class
        
        var font: UIFont!
        
        if AppTarget.isTargetBristolBears() {
            font = UIFont.app_VanguardBoldFontSizeMedium()
        } else {
            font = UIFont.app_avenirNextDemiBoldMediumStratfordRC()
        }
        
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: font,
            textAlignment: .left
        )
        label.textPosition = .default
        //                label.debugView()
        return label
    }
    
    class func label_AppCompetitionCellBody() -> SILabel {
        
        var font: UIFont!
        
        if AppTarget.isTargetBristolBears() {
            font = UIFont.app_InterstateRegularFontSizeMediumSmall()
        } else {
            font = UIFont.app_avenirNextRegularFontMediumSmallStratfordRC()
        }
        
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: font,
            textAlignment: .left
        )
        label.textPosition = .top
        return label
    }
    
    //MARK: - Racecard Cells
    
    class func label_RaceCardCellTitle() -> SILabel {
        var label: SILabel!
        if AppTarget.isTargetSheffieldDogs() {
        label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_avenirNextDemiBoldSmallStratfordRC(),
            textAlignment: .center
        )
        label.textPosition = .default
        }
        return label
    }
        
    // MISC
    class func labelLow6EventRacingTitle() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_HindGunturLightFontSizeMediumSmall(),
            textAlignment: .center
        )
        return label
    }
    
    //MARK: - WhatsOn
    
    class func label_AppBristolCityFCWhatsOnCellTitle() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.BristolCityFC_ButtonColor(),
            numberOfLines: 0,
            font: UIFont.app_HindGunturBoldFontSizeLarge(),
            textAlignment: .left
        )
        label.textPosition = .default
        //                label.debugView()
        return label
    }
    
    class func label_AppBristolCityFCWhatsOnCellBody() -> SILabel {
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_HindGunturLightFontSizeMedium(),
            textAlignment: .left
        )
        label.textPosition = .top
        //        label.debugView()
        return label
    }
    
    
    //MARK: - LOW6 Event

    class func label_Low6EventTitle() -> SILabel {
        
        var textColor: UIColor!
        var font: UIFont!
        
        textColor = UIColor.white
        
        if AppTarget.isTargetBristolBears() {
            font = UIFont.app_VanguardBoldFontSizeMediumSmall()
        } else {
            font = UIFont.app_HindGunturRegularFontSizeMediumSmall()
        }
        
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: textColor,
            numberOfLines: 0,
            font: font,
            textAlignment: .center
        )
        return label
    }
    
    //MARK: - Media Vote
    //titleLabel on best selfie screen
    class func label_AppMediaVoteTitle() -> SILabel {
        let sideInset:CGFloat = K.UI.INSET.SIDE_BORDER
        let sideInsetSmall:CGFloat = K.UI.INSET.SMALL_SIDE_BORDER
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: UIColor.black,
            numberOfLines: 0,
            font: UIFont.app_HindGunturRegularFontSizeMedium(),
            textAlignment: .center)
        
        label.edgeInsets = UIEdgeInsetsMake(sideInset, sideInset, sideInsetSmall, sideInset)
        return label
    }
    
    //Leaderboard Label on best selfie screen
    
    class func label_AppMediaVoteLeaderboard() -> SILabel {
        
        var label: SILabel!
        let sideInset:CGFloat = K.UI.INSET.SIDE_BORDER
        let sideInsetSmall:CGFloat = K.UI.INSET.SMALL_SIDE_BORDER
        
        if AppTarget.isTargetSheffieldDogs() {
            label = AppUIBuilder.getLabel(
                "TOP 3",
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_HindGunturBoldFontSizeLarge(),
                textAlignment: .center
            )
            label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
            label.backgroundColor = UIColor.teamPrimaryColor()
        } else if AppTarget.isTargetBarnsleyFC() {
            label = AppUIBuilder.getLabel(
                "TOP 3",
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_BebasNeueFontSizeLarge(),
                textAlignment: .center
            )
            label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
            label.backgroundColor = UIColor.app_strawberry()
        } else if AppTarget.isTargetBristolBears() {
            label = AppUIBuilder.getLabel(
                "TOP 3",
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_VanguardBoldFontSizeLarge(),
                textAlignment: .center
            )
            label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
            label.backgroundColor = UIColor.black
        }else {
            label = AppUIBuilder.getLabel(
                "TOP 3",
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_BebasNeueFontSizeLarge(),
                textAlignment: .center
            )
            label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
            label.backgroundColor = UIColor.black
        }
    
        return label
        
    }
    
    //Latest Label on best selfie screen
   
    class func label_AppMediaVoteLatest() -> SILabel {
        var label: SILabel!
        let sideInset:CGFloat = K.UI.INSET.SIDE_BORDER
        let sideInsetSmall:CGFloat = K.UI.INSET.SMALL_SIDE_BORDER
        
        if AppTarget.isTargetSheffieldDogs() {
            label = AppUIBuilder.getLabel(
                "MORE",
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_HindGunturBoldFontSizeLarge(),
                textAlignment: .center
            )
            label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
            label.backgroundColor = UIColor.teamPrimaryColor()
        } else if AppTarget.isTargetBarnsleyFC() {
            label = AppUIBuilder.getLabel(
                "MORE",
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_BebasNeueFontSizeLarge(),
                textAlignment: .center
            )
            label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
            label.backgroundColor = UIColor.app_strawberry()
        } else if AppTarget.isTargetBristolBears() {
            label = AppUIBuilder.getLabel(
                "MORE",
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_VanguardBoldFontSizeLarge(),
                textAlignment: .center
            )
            label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
            label.backgroundColor = UIColor.black
        } else {
            label = AppUIBuilder.getLabel(
                "MORE",
                textColor: UIColor.white,
                numberOfLines: 0,
                font: UIFont.app_BebasNeueFontSizeLarge(),
                textAlignment: .center
            )
            label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
            label.backgroundColor = UIColor.black
        }
        
        return label
    }
    
    
    class func label_BctvCategoryTitle() -> SILabel {
        let sideInset:CGFloat = K.UI.INSET.SIDE_BORDER
        let sideInsetSmall:CGFloat = K.UI.INSET.SMALL_SIDE_BORDER
        let  label = AppUIBuilder.getLabel(
            "CATEGORY NAME",
            textColor: UIColor.white,
            numberOfLines: 0,
            font: UIFont.app_BebasNeueFontSizeLarge(),
            textAlignment: .center
        )
        label.edgeInsets = UIEdgeInsetsMake(sideInsetSmall, sideInset, sideInsetSmall, sideInset)
        label.backgroundColor = UIColor.teamPrimaryColor()
        return label
    }

    //MARK: - Prize Draw VC
    
    // Prize Draw title label in Prize Draw vc
    class func label_AppPrizeDrawTitleLabel() -> SILabel {
        
        var font: UIFont!
        var textColor: UIColor!
        
        textColor = UIColor.black
        
        if AppTarget.isTargetBristolBears() {
            font = UIFont.app_VanguardBoldFontSizeMediumSmall()
        } else {
            font = UIFont.app_HindGunturRegularFontSizeMediumSmall()
        }
        
        let label = AppUIBuilder.getLabel(
            nil,
            textColor: textColor,
            numberOfLines: 0,
            font: font,
            textAlignment: .center)
        return label
    }
}
