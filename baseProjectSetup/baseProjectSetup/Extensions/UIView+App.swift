//
//  UIView+App.swift
//  PriortySMS
//
//  Created by Dan Hillman on 26/06/2017.
//  Copyright © 2017 Sync Interactive. All rights reserved.
//

import Foundation

extension UIView {
    
    func springPopAnimation(completion: ((Bool) -> Swift.Void)? = nil) {
        self.scaleDown()
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 5.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        if let weakSelf = self {
                            weakSelf.scaleNormal()
                        }
            },
                       completion: completion)
    }
    
    func scaleDown() {
        self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
    }
    
    func scaleNormal() {
        self.transform = .identity
    }
    
}
