//
//  UILabel+App.swift
//  BristolCityFCPrioritySMS
//
//  Created by Haider Ashfaq on 15/08/2018.
//  Copyright © 2018 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func setDifferentColor(string: String, location: Int, length: Int) {
        let attText = NSMutableAttributedString(string: string)
        attText.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: NSRange(location:location,length:length))
        attributedText = attText
    }
}
