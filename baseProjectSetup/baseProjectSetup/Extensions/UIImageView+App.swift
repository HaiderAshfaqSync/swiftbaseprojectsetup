//
//  UIImageView+App.swift
//  PriortySMS
//
//  Created by Dan Hillman on 05/12/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

extension UIImageView {
    
    func triggerPlaceHolder(_ urlStr:String?) {
//        self.debugView(UIColor.green, width: 2.0)
        if let urlStr = urlStr {
            DebugLog("Error loading image URL: \(urlStr)")
        }
    }
    
    func updateImageFrom(urlStr:String?, placeholderImage:UIImage?, completion: ((_ image:UIImage?, _ isPlaceHolder:Bool) -> Void)? = nil) {
        if let cd_url = urlStr {
            //DebugLog("url: \(cd_url)")
            if let URL = URL(string: cd_url) {
                self.af_cancelImageRequest()
                let urlRequest: URLRequest = {
                    let request = URLRequest(url: URL)
                    return request
                }()
                if let cachedImage = AppImageCacheManager.sharedInstance.imageCache.image(for: urlRequest) {
//                    DebugLog("[Retrieved image from cache: \(urlStr ?? "")]")
//                    DebugLog("[Retrieved cached image at size: \(cachedImage.size) scale: \(cachedImage.scale)]")
                    self.image = cachedImage
                    if let completion = completion {
                        completion(cachedImage, false)
                    }
                } else {
//                    DebugLog("[Downloading image: \(urlStr ?? "")]")
                    let hud = AppHUD.showImageProgressHUDInView(self)
                    self.af_setImage(
                        withURLRequest: urlRequest,
                        placeholderImage: placeholderImage,
                        progress: { [weak self] (progress:Progress) in
                            if let _ = self {
                                DebugLog("[Progress: \(progress)]")
                                DebugLog("[fractionCompleted: \(progress.fractionCompleted)]")
                                DebugLog("[completedUnitCount: \(progress.completedUnitCount)]")
                                
                                if let hud = hud {
                                    hud.progress = Float(progress.fractionCompleted)
                                }
                            }
                        },
                        progressQueue: DispatchQueue.main,
                        imageTransition: .crossDissolve(0.2),
                        completion: { [weak self] (response) in
                            if let weakSelf = self {
                                
                                if let hud = hud {
                                    hud.hide(animated: false)
                                }
                                
                                if let error = response.result.error{
                                    DebugLog("Error: \(error.localizedDescription)")
                                    if error.localizedDescription != "The request was explicitly cancelled." {
                                        weakSelf.triggerPlaceHolder(urlStr)
                                    }
                                    
                                    if let completion = completion {
                                        completion(placeholderImage, true)
                                    }
                                } else {
                                    if let resultImage = response.result.value {
                                        DebugLog("[Storing image into cache: \(urlStr ?? "")]")
                                        DebugLog("[Storing image at size: \(resultImage.size) scale: \(resultImage.scale)]")
                                        AppImageCacheManager.sharedInstance.imageCache.add(resultImage, for: urlRequest)
                                        if let completion = completion {
                                            completion(resultImage, false)
                                        }
                                    } else {
                                        DebugLog("Error: Result Image is nil")
                                        weakSelf.triggerPlaceHolder(urlStr)
                                        if let completion = completion {
                                            completion(placeholderImage, true)
                                        }
                                    }
                                }
                            }
                        }
                    )
                }
            } else {
                self.image = placeholderImage
                if let completion = completion {
                    completion(placeholderImage, true)
                }
            }
        } else {
            self.image = placeholderImage
            if let completion = completion {
                completion(placeholderImage, true)
            }
        }
    }
    
}
