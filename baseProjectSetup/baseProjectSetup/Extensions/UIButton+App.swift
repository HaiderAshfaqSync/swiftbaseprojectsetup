//
//  UIButton+App.swift
//  PriortySMS
//
//  Created by Dan Hillman on 16/05/2017.
//  Copyright © 2017 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func styleButton() {
        self.layer.cornerRadius = CGFloat(K.UI.CORNER_RADIUS.BUTTON)
        self.layer.masksToBounds = true
		styleAttributedTitle(UIColor.black, state:.normal)
        self.setBackgroundImage(UIImage.imageWithColor(UIColor.red), for: UIControlState())
    }
    
    func styleAttributedTitle(_ color: UIColor, state: UIControlState, font: UIFont = UIFont.systemFont(ofSize: 16.0)) {
        let attrs = [
            NSFontAttributeName : font,
            NSForegroundColorAttributeName : color
        ]
        styleAttributedTitle(attrs, state: state)
    }
    
    // MARK: - Image Downloading
    
    func triggerPlaceHolder(_ urlStr:String?) {
        self.debugView(UIColor.green, width: 2.0)
        if let urlStr = urlStr {
            DebugLog("Error loading image URL: \(urlStr)")
        }
    }
}
