//
//  UITableViewCell+App.swift
//  BristolCityFCPrioritySMS
//
//  Created by Haider Ashfaq on 07/12/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    
//    func styleAccessoryChevronRight(_ colour:UIColor = UIColor(207, 207, 207, 1.0)) {
//        let iconSize:CGFloat = CGFloat(K.UI.SIZE.TABLE_CHEVRON)
//        let image = UIImage.imageWithFontAwesome(.ChevronRight,
//                                                 color: colour,
//                                                 iconSize: iconSize,
//                                                 imageSize: CGSize(width: iconSize, height: iconSize)).withRenderingMode(.alwaysOriginal)
//        let imageView = UIImageView(image: image)
//        self.accessoryView = imageView
//    }
//    
    func styleAccessoryChevronDown() {
        let image = UIImage(named: "menu-arrow-open-section")?.withRenderingMode(.alwaysOriginal)
        let imageView = UIImageView(image: image)
        self.accessoryView = imageView
    }
    
    func styleAccessoryChevronUp() {
        let image = UIImage(named: "menu-arrow-close-section")?.withRenderingMode(.alwaysOriginal)
        let imageView = UIImageView(image: image)
        self.accessoryView = imageView
    }
//
//    func styleAccessoryCountLabel(
//        _ text:String? = nil,
//        backgroundColour:UIColor = UIColor.tm_Seperator()
//        ) {
//        
//        let countLabel = SILabel(frame: CGRect(x: 0, y: 0, width: 30.0, height: 30.0))
//        countLabel.edgeInsets = UIEdgeInsetsMake(2, 2, 2, 2)
//        countLabel.text = text
//        countLabel.numberOfLines = 1
//        countLabel.font = UIFont.robotoMediumMedium()
//        countLabel.textAlignment = .center
//        countLabel.textColor = UIColor.white
//        countLabel.backgroundColor = backgroundColour
//        countLabel.setMinimumFontSize_si(8.0)
//        countLabel.layer.cornerRadius = countLabel.height / 2
//        countLabel.layer.masksToBounds = true
//        self.accessoryView = countLabel
//    }
    
}
