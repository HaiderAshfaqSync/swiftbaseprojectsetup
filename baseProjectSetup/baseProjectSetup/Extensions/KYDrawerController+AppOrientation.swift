//
//  KYDrawerController+App.swift
//  TalkingMotors
//
//  Created by Dan Hillman on 11/10/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import KYDrawerController

extension KYDrawerController {
    
    // MARK: Orientation
    
    open override var shouldAutorotate : Bool {
        if let mainViewController = mainViewController {
            if mainViewController is UINavigationController {
                let nc = mainViewController as! UINavigationController
                return nc.shouldAutorotate
            } else {
                return mainViewController.shouldAutorotate
            }
        }
        
        return true
    }
    
    open override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if let mainViewController = mainViewController {
            if mainViewController is UINavigationController {
                let nc = mainViewController as! UINavigationController
                return nc.supportedInterfaceOrientations
            } else {
                return mainViewController.supportedInterfaceOrientations
            }
        }
        
        return .portrait
    }
    
    open override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        if let mainViewController = mainViewController {
            if mainViewController is UINavigationController {
                let nc = mainViewController as! UINavigationController
                return nc.preferredInterfaceOrientationForPresentation
            } else {
                return mainViewController.preferredInterfaceOrientationForPresentation
            }
        }
        
        return .portrait
    }
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        if let mainViewController = mainViewController {
            if mainViewController is UINavigationController {
                let nc = mainViewController as! UINavigationController
                return nc.preferredStatusBarStyle
            } else {
                return mainViewController.preferredStatusBarStyle
            }
        }
        return .lightContent
    }
    
}
