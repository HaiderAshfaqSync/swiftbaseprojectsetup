//
//  UITextField+App.swift
//  PriortySMS
//
//  Created by Dan Hillman on 06/12/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func stylePlaceholder(_ placeholder:String?) {
        self.attributedPlaceholder = NSAttributedString(
            string:placeholder ?? "",
            attributes:[NSForegroundColorAttributeName: UIColor.darkGray]
        )
    }
    
    func styleStandard() {
        self.textColor = UIColor.black
        self.textAlignment = .center
        self.contentVerticalAlignment = .center
        self.contentHorizontalAlignment = .center
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
        self.textColor = UIColor.black
        self.clearButtonMode = .whileEditing
        self.borderStyle = .none
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = CGFloat(K.UI.WIDTH.TEXTFIELD_BORDER)
        self.layer.cornerRadius = CGFloat(K.UI.CORNER_RADIUS.TEXTFIELD)
    }
    
    func styleWithLeftIcon(_ appFontIcon:AppFontIcon, colour:UIColor = UIColor.black) {
        let iconSize:CGFloat = CGFloat(K.UI.SIZE.TEXTFIELD_LEFT_ICON)
        let image = UIImage.imageWithAppIcon(appFontIcon,
                                                 color: colour,
                                                 iconSize: iconSize,
                                                 imageSize: CGSize(width: iconSize, height: iconSize)).withRenderingMode(.alwaysOriginal)
        
        let leftImageView = AppUIBuilder.getImageView(image)
        self.leftViewMode = .always
        self.leftView = leftImageView
        
        let rightImageView = AppUIBuilder.getImageView(image)
        rightImageView.isHidden = true
        self.rightViewMode = .always
        self.rightView = rightImageView
    }
    
    func styleWithLeftIcon(_ leftIcon:AppFontIcon, rightIcon:AppFontIcon, leftColour:UIColor, rightColour:UIColor) {
        let leftIconSize:CGFloat = CGFloat(K.UI.SIZE.TEXTFIELD_LEFT_ICON)
        let leftImage = UIImage.imageWithAppIcon(leftIcon,
                                                 color: leftColour,
                                                 iconSize: leftIconSize,
                                                 imageSize: CGSize(width: leftIconSize, height: leftIconSize)).withRenderingMode(.alwaysOriginal)
        
        let leftImageView = AppUIBuilder.getImageView(leftImage)
        self.leftViewMode = .always
        self.leftView = leftImageView
        
        let rightIconSize:CGFloat = CGFloat(K.UI.SIZE.TEXTFIELD_RIGHT_ICON)
        let rightImage = UIImage.imageWithAppIcon(rightIcon,
                                                     color: rightColour,
                                                     iconSize: rightIconSize,
                                                     imageSize: CGSize(width: rightIconSize, height: rightIconSize)).withRenderingMode(.alwaysOriginal)
        
        let rightImageView = AppUIBuilder.getImageView(rightImage)
        self.rightViewMode = .always
        self.rightView = rightImageView
    }
    
}
