//
//  UIAlertController+App.swift
//  TalkingMotors
//
//  Created by Dan Hillman on 13/02/2017.
//  Copyright © 2017 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    
    func removeTextfieldsBorderVisualEffect() {
        if let textFields = self.textFields {
            for textfield: UIView in textFields {
                if let container: UIView = textfield.superview {
                    if let containerSuperview = container.superview {
                        for view in containerSuperview.subviews {
                            if view is UIVisualEffectView {
                                let effectView = view as! UIVisualEffectView
                                container.backgroundColor = UIColor.clear
                                effectView.removeFromSuperview()
                            }
                        }
                    }
                }
            }
        }
    }
}
