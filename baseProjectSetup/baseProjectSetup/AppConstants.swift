//
//  AppConstants.swift
//  baseProjectSetup
//
//  Created by Haider Ashfaq on 20/08/2018.
//  Copyright © 2018 Haider Ashfaq. All rights reserved.
//

import Foundation

struct K {
    struct UI {
        
        struct WIDTH {
            static let TEXTFIELD_BORDER:CGFloat = 1.0
            static let BUTTON_BORDER:CGFloat = 1.0
            static let SCREEN:CGFloat = UIScreen.main.bounds.size.width
        }
        
        struct HEIGHT {
            static let TEXTFIELD:CGFloat = 36.0
            static let BUTTON:CGFloat = 50.0
            static let MENU_CELL:CGFloat = 50.0
            static let PROFILE_IMAGE:CGFloat = 80.0
            static let AUTH_HEADER:CGFloat = 200.0
            static let SUB_HEADER:CGFloat = 50.0
            static let SINGLE_LINE_VIEW:CGFloat = 30.0
            static let HOME_MENU_TALL:CGFloat = 300.0
            static let HOME_MENU_SHORT:CGFloat = HOME_MENU_TALL * 0.5
            static let HOME_MENU_TITLE:CGFloat = 60.0
            static let FILTERS_CAROUSEL:CGFloat = 60.0
            static let SOCIAL_HEADER:CGFloat = 50.0
        }
        
        struct SIZE {
            static let UPLOAD_IMAGE:CGSize = CGSize(width: 2048, height: 2048)
            static let NAV_ICON:CGFloat = 22.0
            static let MENU_ICON:CGFloat = 25.0
            static let TEXTFIELD_LEFT_ICON = HEIGHT.TEXTFIELD * 0.8
            static let TEXTFIELD_RIGHT_ICON = TEXTFIELD_LEFT_ICON
            static let PHOTO_IMAGE:CGFloat = 100.0
            static let NAV_BUTTON:CGFloat = 30.0
            static let MENU_ITEM_ICON:CGFloat = 30.0
            static let EDS_ICON:CGFloat = 50.0
            static let CELL_PLACEHOLDER_IMAGE:CGFloat = 40.0
            static let CAMERA_MODE_BUTTON:CGFloat = 30.0
            static let TICK_COMPETITION:CGFloat = 25.0
            static let VIDEO_ICON:CGFloat = 25.0
            static let VIDEO_ICON_LARGE:CGFloat = 60.0
            static let VOTE_BUTTON:CGFloat = 60.0
            static let CIRCLE_RANK:CGFloat = 50.0
            static let CIRCLE_LOW6_CLOTH_NO:CGFloat = 25.0
            static let CIRCLE_RANK_INSET:CGFloat = CIRCLE_RANK * 0.75
            static let CIRCLE_KEY_EVENTS_CELL:CGFloat = 25.0
            static let LEFT_MENU_ICONS:CGFloat = 20.0
            static let CELL_ACCESSORY:CGFloat = 10.0
            static let MENU_CHEVRON = 22.0
        }
        
        struct CORNER_RADIUS {
            static let TEXTFIELD:CGFloat = 5.0
            static let BUTTON:CGFloat = 5.0
        }
        
        struct INSET {
            static let SIDE_BORDER:CGFloat = 20.0
            static let LARGE_SIDE_BORDER:CGFloat = SIDE_BORDER * 2.0
            static let SMALL_SIDE_BORDER:CGFloat = SIDE_BORDER * 0.5
            static let EXTRASMALL_SIDE_BORDER:CGFloat = SIDE_BORDER * 0.25
            static let COLLECTION_ICON:CGFloat = 2.0
        }
        
        struct ALPHA {
            static let MENU_ITEM:CGFloat = 0.8
        }
    }
    
    struct UI_BLACKBURN_ROVERS {
        
        struct SIZE {
            static let LEFT_MENU_ICONS:CGFloat = 22.0
        }
        
        struct INSET {
            static let SIDE_BORDER:CGFloat = 30.0
        }
    }
    
    struct DURATION {
        
    }
    
    struct NUM {
        static let RETRY_LOGIN_ATTEMPTS: Int = 3
        static let MAX_VIDEO_LENGTH_SECONDS: Int = 20
        static let MAX_CHARACTERS_NAME: Int = 140
        static let MAX_CHARACTERS_CAPTION: Int = 255
        
        // The number of times we should retry the login call to refresh the token
        static let MAX_REQUEST_TOKEN_REFRESH_RETRY_COUNT:Int = 2
        
        // The number of times we should retry the previous failed calls AFTER refreshing the token
        static let MAX_REQUEST_RETRY_COUNT:Int = 2
    }
    
    struct ZOOM_LEVELS {
        static let MIN_ZOOM = CGFloat(1.0)
        static let MAX_ZOOM = CGFloat(5.0)
        static let START_ZOOM = CGFloat(1.0)
    }
}
