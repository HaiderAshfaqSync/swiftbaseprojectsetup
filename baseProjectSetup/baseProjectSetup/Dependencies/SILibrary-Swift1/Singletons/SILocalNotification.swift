//
//  SILocalNotification.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 19/08/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import AVKit

class SILocalNotification: NSObject {
    
    let LOCAL_NOTIFICATION_CATEGORY : String = "SILocalNotificationCategory"
    
    // MARK: - Shared Instance
    
    static let sharedInstance = SILocalNotification()

    // MARK: - Schedule Notification
    
    func scheduleNotificationWithKey(_ key: String, title: String, message: String, seconds: Double, userInfo: [AnyHashable: Any]? = nil, repeatInterval: NSCalendar.Unit? = nil) {
        let date = Date(timeIntervalSinceNow: TimeInterval(seconds))
        let notification = notificationWithTitle(key, title: title, message: message, date: date, userInfo: userInfo, soundName: nil, hasAction: true, repeatInterval: repeatInterval)
        notification.category = LOCAL_NOTIFICATION_CATEGORY
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    func scheduleNotificationWithKey(_ key: String, title: String, message: String, date: Date, userInfo: [AnyHashable: Any]? = nil, repeatInterval: NSCalendar.Unit? = nil){
        let notification = notificationWithTitle(key, title: title, message: message, date: date, userInfo: ["key": key], soundName: nil, hasAction: true, repeatInterval: repeatInterval)
        notification.category = LOCAL_NOTIFICATION_CATEGORY
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    func scheduleNotificationWithKey(_ key: String, title: String, message: String, seconds: Double, soundName: String, userInfo: [AnyHashable: Any]? = nil, repeatInterval: NSCalendar.Unit? = nil){
        let date = Date(timeIntervalSinceNow: TimeInterval(seconds))
        let notification = notificationWithTitle(key, title: title, message: message, date: date, userInfo: ["key": key], soundName: soundName, hasAction: true, repeatInterval: repeatInterval)
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    func scheduleNotificationWithKey(_ key: String, title: String, message: String, date: Date, soundName: String, userInfo: [AnyHashable: Any]? = nil, repeatInterval: NSCalendar.Unit? = nil){
        let notification = notificationWithTitle(key, title: title, message: message, date: date, userInfo: ["key": key], soundName: soundName, hasAction: true, repeatInterval: repeatInterval)
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    // MARK: - Present Notification
    
    func presentNotificationWithKey(_ key: String, title: String, message: String, soundName: String, userInfo: [AnyHashable: Any]? = nil, repeatInterval: NSCalendar.Unit? = nil) {
        let notification = notificationWithTitle(
            key,
            title: title,
            message: message,
            date: nil,
            userInfo: ["key": key],
            soundName: nil,
            hasAction: true,
            repeatInterval: repeatInterval
        )
        UIApplication.shared.presentLocalNotificationNow(notification)
    }
    
    // MARK: - Create Notification
    
    func notificationWithTitle(
        _ key : String,
        title: String,
        message: String,
        date: Date?,
        userInfo: [AnyHashable: Any]?,
        soundName: String?,
        hasAction: Bool,
        repeatInterval: NSCalendar.Unit? = nil
        ) -> UILocalNotification {
        
        var dct : Dictionary<String,Any> = userInfo as! Dictionary<String,Any>
        dct["key"] = NSString(string: key) as String as String as Any?
        
        let notification = UILocalNotification()
        notification.alertTitle = title
        notification.alertBody = message
        notification.userInfo = dct
        notification.soundName = soundName ?? UILocalNotificationDefaultSoundName
        notification.fireDate = date
        notification.hasAction = hasAction
        
        if let repeatInterval = repeatInterval {
            notification.repeatInterval = repeatInterval
        }
        
        return notification
    }
    
    func getNotificationWithKey(_ key : String) -> UILocalNotification {
        
        var notif : UILocalNotification?
        
        for notification in UIApplication.shared.scheduledLocalNotifications! where notification.userInfo!["key"] as! String == key{
            notif = notification
            break
        }
        
        return notif!
    }
    
    func cancelNotification(_ key : String){
        
        for notification in UIApplication.shared.scheduledLocalNotifications! where notification.userInfo!["key"] as! String == key{
            UIApplication.shared.cancelLocalNotification(notification)
            break
        }
    }
    
    func getAllNotifications() -> [UILocalNotification]? {
        return UIApplication.shared.scheduledLocalNotifications
    }
    
    func cancelAllNotifications() {
        UIApplication.shared.cancelAllLocalNotifications()
    }
    
    func registerUserNotificationWithActionButtons(actions : [UIUserNotificationAction]){
        
        let category = UIMutableUserNotificationCategory()
        category.identifier = LOCAL_NOTIFICATION_CATEGORY
        
        category.setActions(actions, for: UIUserNotificationActionContext.default)
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: NSSet(object: category) as? Set<UIUserNotificationCategory>)
        UIApplication.shared.registerUserNotificationSettings(settings)
    }
    
    func registerUserNotification(){
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
    }
    
    func createUserNotificationActionButton(identifier : String, title : String) -> UIUserNotificationAction{
        
        let actionButton = UIMutableUserNotificationAction()
        actionButton.identifier = identifier
        actionButton.title = title
        actionButton.activationMode = UIUserNotificationActivationMode.background
        actionButton.isAuthenticationRequired = true
        actionButton.isDestructive = false
        
        return actionButton
    }
    
}
