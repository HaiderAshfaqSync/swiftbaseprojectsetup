//
//  SIImagePicker.swift
//  TalkingMotors
//
//  Created by Dan Hillman on 09/08/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import TOCropViewController

class SIImagePicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate, TOCropViewControllerDelegate {
    static let sharedInstance = SIImagePicker()
    
    var presenter:UIViewController!
    var imagePickerController:UIImagePickerController?
    var imagePickedBlock : ((_ editedImage:UIImage, _ picker:UIImagePickerController?, _ info:NSDictionary?) -> Void)?
    
    fileprivate override init() {
        
    }
    
    deinit {
        cleanup()
    }
    
    internal func openPhotoPicker(
        _ sourceType:UIImagePickerControllerSourceType,
        sender:Any,
        popOverOrigin:Any,
        popOverInView:UIView,
        presenter:UIViewController,
        stylePickerBlock: (_ picker:UIImagePickerController) ->(),
        imagePickedBlock: @escaping (_ editedImage:UIImage, _ picker:UIImagePickerController?, _ info:NSDictionary?) ->()
        ) {
        
        if (UIImagePickerController.isSourceTypeAvailable(sourceType)) {
            let authStatus:AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
            if ((authStatus == .denied || authStatus == .restricted)
                &&
                sourceType == .camera) {
                
                presenter.alert(Localised.key("take_a_photo"),
                                message: Localised.key("take_a_photo_restricted"),
                                buttonTitle: Localised.key("ok"))
                
            } else if ((authStatus == .denied || authStatus == .restricted)
                &&
                (sourceType == .photoLibrary || sourceType == .savedPhotosAlbum)) {
                
                presenter.alert(Localised.key("camera_roll"),
                                message: Localised.key("camera_roll_restricted"),
                                buttonTitle: Localised.key("ok"))
                
            } else {
                let availableMediaTypes:NSArray = UIImagePickerController.availableMediaTypes(for: sourceType)! as NSArray
                if (availableMediaTypes.contains(kUTTypeImage as String)) {
                    
                    cleanup()
                    self.imagePickedBlock = imagePickedBlock
                    imagePickerController = UIImagePickerController()
                    
                    if let imagePickerController = imagePickerController {
                        imagePickerController.modalPresentationStyle = .fullScreen
                        imagePickerController.sourceType = sourceType
                        imagePickerController.mediaTypes = [kUTTypeImage as String]
                        imagePickerController.allowsEditing = false
                        imagePickerController.delegate = self
                        imagePickerController.navigationBar.isTranslucent = false
//                        imagePickerController.navigationBar.barTintColor = .redColor()      // Bar colour
//                        imagePickerController.navigationBar.tintColor = .whiteColor()       // Cancel button item colour
//                        imagePickerController.navigationBar.titleTextAttributes = [
//                            NSForegroundColorAttributeName : UIColor.whiteColor()
//                        ]                                                                   // Title color
                        
                        stylePickerBlock(imagePickerController)
                        
                        self.presenter = presenter
                        
                        if (SI.PLATFORM.IS_IPAD) {
                            imagePickerController.modalPresentationStyle = .popover
                            if (popOverOrigin is UIBarButtonItem) {
                                let barButtonItem:UIBarButtonItem = popOverOrigin as! UIBarButtonItem
                                imagePickerController.popoverPresentationController?.barButtonItem = barButtonItem
                                
                            } else if (popOverOrigin is UIView) {
                                imagePickerController.popoverPresentationController?.sourceView = popOverOrigin as? UIView
                            }
                            presenter.present(imagePickerController, animated: true, completion: nil)
                            
                        } else {
                            presenter.present(imagePickerController, animated: true, completion: nil)
                        }
                    }
                }
            }
        } else {
            if (sourceType == .camera) {
                presenter.alert(Localised.key("take_a_photo"),
                                message: Localised.key("take_a_photo_unsupported"),
                                buttonTitle: Localised.key("ok"))
            } else {
                presenter.alert(Localised.key("camera_roll"),
                                message: Localised.key("camera_roll_unsupported"),
                                buttonTitle: Localised.key("ok"))
            }
        }
    }
    
    func cleanup() {
        self.presenter = nil
        
        if ((self.imagePickedBlock) != nil) {
            self.imagePickedBlock = nil
        }
        
        if ((self.imagePickerController) != nil) {
            self.imagePickerController = nil
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let image = originalImage {
            originalImage = image.fixedOrientation()
        }
        logOutput(image: originalImage)
        picker.dismiss(animated: true) {
            [unowned self] in
            self.showCrop(image:originalImage!)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            [unowned self] in
            self.cleanup()
        }
    }
    
    // MARK: - Private
    
    private func logOutput(image:UIImage?) {
        if let image = image {
            if SI.PLATFORM.IS_SIMULATOR {
                print("Width: \(image.size.width) Height: \(image.size.height)")
                
                if let imageData = UIImageJPEGRepresentation(image, CGFloat(1.0)) {
                    let imageSize: Float = Float(imageData.count)
                    let imageSizeKb = imageSize / 1024.0
                    print("FileSize KB: \(imageSizeKb)")
                }
            }
        }
    }
    
    // MARK: - Crop Image
    
    private func showCrop(image:UIImage) {
        let vc = TOCropViewController(image: image)
        vc.delegate = self
        vc.showActivitySheetOnDone = false
        vc.aspectRatioPreset = .preset16x9
        vc.aspectRatioLockEnabled = true
        vc.resetAspectRatioEnabled = false
        vc.rotateButtonsHidden = true
        presenter.present(vc, animated: true) {
            
        }
    }
    
    // MARK: - Crop Delegate
    
    public func cropViewController(_ cropViewController: TOCropViewController, didCropToRect cropRect: CGRect, angle: Int) {
        print("didCropToRect")
    }
    
    public func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        print("didCropToImage")
        
        logOutput(image: image)
        
        if (self.imagePickedBlock != nil) {
            self.imagePickedBlock!(image, nil, nil)
        }
        
        cropViewController.dismiss(animated: true) {
            [unowned self] in
            self.cleanup()
        }
    }
    
    public func cropViewController(_ cropViewController: TOCropViewController, didCropToCircleImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        // Not used
        print("didCropToCircleImage")
    }
    
    public func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        print("didFinishCancelled: \(cancelled)")
        cropViewController.dismiss(animated: true) { 
            [unowned self] in
            self.cleanup()
        }
    }
    
}
