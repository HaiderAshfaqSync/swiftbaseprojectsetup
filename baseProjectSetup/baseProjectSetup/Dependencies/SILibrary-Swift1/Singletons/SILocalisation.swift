//
//  SILocalisation.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 04/05/2016.
//  Copyright © 2016 Sync Interactive Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class SILocalisation: NSObject {
    static let sharedInstance = SILocalisation()
    
    var myBundle: Bundle = {
        let cultureCode = SILocalisation.getCurrentCultureCode()
        DebugLog("INITIALIZE WITH CULTURE CODE: \(cultureCode)")
        return SILocalisation.getBundleFor(cultureCode:cultureCode)
    }()
    
    fileprivate override init() {
        super.init()
    }
    
    deinit {
        DebugLog("DEALLOC")
    }
    
    // MARK: - Public
    
    class func getDefaultCultureCode() -> String {
        return "en-GB"
    }
    
    class func getLocalisedValueForKey(_ key:String) -> String {
        return SILocalisation.sharedInstance.getLocalisedValueForKey(key)
    }
    
    class func setCurrentCultureCode(_ cultureCode:String) {
        return SILocalisation.sharedInstance.setCurrentCultureCode(cultureCode)
    }
    
    class func getBundleFor(cultureCode:String) -> Bundle {
        if let path = Bundle.main.path(forResource: cultureCode, ofType: "lproj") {
            if let bundle = Bundle(path: path) {
                return bundle
            } else {
                return Bundle.main
            }
        } else {
            return Bundle.main
        }
    }
    
    // MARK: - Private
    
    fileprivate func setLanguage(_ cultureCode:String) {
        myBundle = SILocalisation.getBundleFor(cultureCode:cultureCode)
    }
    
    fileprivate func getLocalisedValueForKey(_ key:String) -> String {
        return myBundle.localizedString(forKey: key, value: "", table: nil)
    }
    
    // MARK: - Storage (Culture Code)
    
    class func getCurrentCultureCodeKey() -> String {
        return "SILocalisation-getCurrentCultureCodeKey"
    }
    
    class func getCurrentCultureCode() -> String {
        let cultureCode:String? = UserDefaults.objectForKey(SILocalisation.getCurrentCultureCodeKey()) as? String
        if let cultureCode = cultureCode {
            return cultureCode
        } else {
            let key = "app_culture_code"
            let value = NSLocalizedString(key, comment: "")
            if value == key {
                return SILocalisation.getDefaultCultureCode()
            } else  {
                return value
            }
        }
    }

    fileprivate func setCurrentCultureCode(_ cultureCode:String) {
        self.setLanguage(cultureCode)
        UserDefaults.setObject(cultureCode, key: SILocalisation.getCurrentCultureCodeKey())
    }
}
