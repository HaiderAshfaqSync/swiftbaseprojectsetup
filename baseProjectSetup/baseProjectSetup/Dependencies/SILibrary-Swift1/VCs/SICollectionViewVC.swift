//
//  SICollectionViewVC.swift
//  MozaicCollectionViewTest
//
//  Created by Haider Ashfaq on 29/11/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet

class SICollectionViewVC: SIRootVC, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, SIProtocolEDS {

    
    // MARK: - Variables/Constants
    
    public var collectionView: SICollectionView?
    weak internal var edsDelegate: SIProtocolEDS!
    public var refreshControl: UIRefreshControl?
    
    // MARK: - Initialization
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    deinit {
//        DebugLog("DEALLOC")
        
        if let collectionView = collectionView {
            collectionView.dataSource = nil
            collectionView.delegate = nil
            edsDelegate = nil
            collectionView.emptyDataSetSource = nil
            collectionView.emptyDataSetDelegate = nil
        }
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        super.loadView()
        
        setupCollectionView(layout:getCollectionViewLayout())
        if let collectionView = collectionView {
            self.view.addSubview(collectionView)
            setupCollectionViewConstraints()
        }
        
        self.view.setNeedsUpdateConstraints()
    }
    
    // MARK: - UI
    
    func setupCollectionView(layout:UICollectionViewLayout) {
        collectionView = SICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        if let collectionView = collectionView {
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            collectionView.delegate = self
            collectionView.dataSource = self
            edsDelegate = self
            collectionView.emptyDataSetSource = self
            collectionView.emptyDataSetDelegate = self
            collectionView.scrollsToTop = true
            collectionView.delaysContentTouches = false
            collectionView.canCancelContentTouches = true
            collectionView.keyboardDismissMode = .interactive
            collectionView.backgroundColor = UIColor.clear
            collectionView.alwaysBounceVertical = true
            collectionView.showsVerticalScrollIndicator = false
            collectionView.showsHorizontalScrollIndicator = false
            collectionView.contentInset = UIEdgeInsets.zero
            registerViews(forCollectionView: collectionView)
        }
    }
    
    func setupCollectionViewConstraints() {
        if let collectionView = collectionView {
            collectionView.autoPinEdgesToSuperviewEdges()
        }
    }
    
    func getCollectionViewLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = getMinimumLineSpacing()
        layout.minimumInteritemSpacing = getMinimumInteritemSpacing()
        layout.itemSize = getItemSize()
        layout.sectionInset = getSectionInset()
        layout.headerReferenceSize = getHeaderReferenceSize()
        layout.footerReferenceSize = getFooterReferenceSize()
        return layout
    }
    
    func registerViews(forCollectionView:UICollectionView) {
        forCollectionView.register(SICollectionViewCell.self, forCellWithReuseIdentifier: "SICollectionViewCell")
    }
    
    // MARK: - Public Helpers
    
    func scrollToIndexPath(_ indexPath:IndexPath, scrollPosition:UICollectionViewScrollPosition, animated:Bool) {
        if let collectionView = collectionView {
            collectionView.scrollToItem(at: indexPath, at: scrollPosition, animated: animated)
        }
    }
    
    func scrollToTop(_ scrollPosition:UICollectionViewScrollPosition, animated:Bool) {
        if let collectionView = collectionView {
            if let firstIndexPath = collectionView.getFirstIndexPath() {
                scrollToIndexPath(firstIndexPath as IndexPath, scrollPosition: scrollPosition, animated: animated)
            }
        }
    }
    
    func scrollToBottom(_ scrollPosition:UICollectionViewScrollPosition, animated:Bool) {
        if let collectionView = collectionView {
            if let lastIndexPath = collectionView.getLastIndexPath() {
                scrollToIndexPath(lastIndexPath as IndexPath, scrollPosition: scrollPosition, animated: animated)
            }
        }
    }
    
    // MARK: - Public Data
    
    func getMinimumLineSpacing() -> CGFloat {
        return 0.0
    }
    
    func getMinimumInteritemSpacing() -> CGFloat {
        return 0.0
    }
    
    func getItemWidth() -> CGFloat {
        let minimumInteritemSpacing = getMinimumInteritemSpacing()
        let numColumns = getNumColumns()
        let fillWidth = self.view.frame.width == 0 ? SI.SCREEN_SIZE.SCREEN_WIDTH : self.view.frame.width
        let remainingWidth = (fillWidth - (CGFloat(numColumns + 1) * minimumInteritemSpacing))
        let itemWidth = remainingWidth / CGFloat(numColumns)
        return itemWidth
    }
    
    func getItemHeight() -> CGFloat {
        return getItemWidth()
    }
    
    func getNumColumns() -> Int {
        return 2
    }
    
    func getSectionInset() -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func getItemSize() -> CGSize {
        return CGSize(width: getItemWidth(), height: getItemHeight())
    }
    
    func getHeaderReferenceSize() -> CGSize {
        return CGSize.zero
    }
    
    func getFooterReferenceSize() -> CGSize {
        return CGSize.zero
    }
    
    func getNumSections() -> Int {
        return 0
    }
    
    func getNumberOfRowsInSection(_ section: Int) -> Int {
        return 0
    }
    
    func getObjectAtIndexPath(_ indexPath:IndexPath) -> Any? {
        return nil
    }
    
    func reloadData() {
        if let collectionView = collectionView {
            collectionView.reloadData()
        }
    }
    
    func reloadEmptyDataSet() {
        if let collectionView = collectionView {
            collectionView.reloadEmptyDataSet()
        }
    }
    
    // MARK: - Public Refresh Control
    
    @discardableResult func addRefreshControl(_ title:String = "", backgroundColor:UIColor, tintColour:UIColor, selector:Selector) -> UIRefreshControl? {
        if ((refreshControl == nil)) {
            refreshControl = UIRefreshControl()
        }
        
        refreshControl?.addTarget(self, action: selector, for: .valueChanged)
        refreshControl?.backgroundColor = backgroundColor
        refreshControl?.tintColor = tintColour
        refreshControl?.attributedTitle = (NSAttributedString(string: title, attributes: [
            NSForegroundColorAttributeName : tintColour
            ]))
        if let collectionView = collectionView {
            collectionView.backgroundView = refreshControl
        }
        return refreshControl
    }
    
    func beginRefreshing() {
        if ((refreshControl) != nil) {
            UIView.animate(withDuration: 0.0,
                           delay: 0.0,
                           options: .beginFromCurrentState,
                           animations: { [weak self] in
                            if let weakSelf = self {
                                if let collectionView = weakSelf.collectionView {
                                    collectionView.contentOffset = CGPoint(x: 0, y: -weakSelf.refreshControl!.frame.size.height)
                                }
                            }
                }, completion: { [weak self] (finished:Bool) in
                    if let weakSelf = self {
                        weakSelf.refreshControl!.beginRefreshing()
                    }
            })
        }
    }
    
    func endRefreshWithTimestampText(_ text:String, dateFormat:String = "MMM d, h:mm a", textColour:UIColor) {
        if ((refreshControl) != nil) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            let title = text + dateFormatter.string(from: Date())
            let attributedTitle = NSAttributedString(string: title, attributes: [
                NSForegroundColorAttributeName : textColour
                ])
            refreshControl!.attributedTitle = attributedTitle
            endRefreshing()
        }
    }
    
    func endRefreshing() {
        if ((refreshControl) != nil) {
            refreshControl!.endRefreshing()
        }
    }
    
    // MARK: - CollectionView Datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return getNumSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getNumberOfRowsInSection(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SICollectionViewCell", for: indexPath) as! SICollectionViewCell
//        cell.contentView.debugView()
        return cell
    }
    
    // MARK: - CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        print("Pressed: \(indexPath)")
    }
    
    // MARK: - Observing
    
    override func addObserversUntilDisappear() {
        if (!self.isObservingUntilDisappear) {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.keyboardWillShowNotification),
                                                   name: NSNotification.Name.UIKeyboardWillShow,
                                                   object: nil)
            
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.keyboardWillHideNotification),
                                                   name: NSNotification.Name.UIKeyboardWillHide,
                                                   object: nil)
        }
        
        super.addObserversUntilDisappear()
    }
    
    override func removeObserversUntilDisappear() {
        if (self.isObservingUntilDisappear) {
            NotificationCenter.default.removeObserver(self,
                                                      name: NSNotification.Name.UIKeyboardWillShow,
                                                      object: nil)
            
            NotificationCenter.default.removeObserver(self,
                                                      name: NSNotification.Name.UIKeyboardWillHide,
                                                      object: nil)
        }
        
        super.removeObserversUntilDisappear()
    }
    
    // MARK: - NOTIFICATIONS
    
    @objc func keyboardWillShowNotification(_ notification:Notification) {
        if let collectionView = collectionView {
            if let userInfo = notification.userInfo {
                if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                    let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                    collectionView.contentInset = contentInsets
                    collectionView.scrollIndicatorInsets = contentInsets
                }
            }
        }
    }
    
    @objc func keyboardWillHideNotification(_ notification:Notification) {
        if let collectionView = collectionView {
            let contentInsets = UIEdgeInsets.zero
            collectionView.contentInset = contentInsets
            collectionView.scrollIndicatorInsets = contentInsets
        }
    }
    
}
