//
//  SIRootVC.swift
//  MozaicCollectionViewTest
//
//  Created by Haider Ashfaq on 29/11/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation
import PureLayout

class SIRootVC: UIViewController {
    
    // MARK: - Variables/Constants
    
    var isObservingUntilDeinit:Bool = false
    var isObservingUntilDisappear:Bool = false
    var isRequestingData:Bool = false
    
    let bgView: UIView = {
        let view = UIView.newAutoLayout()
        view.backgroundColor = UIColor.black
        return view
    }()
    
    // MARK: - Initialization
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        addObserversUntilDeinit()
    }
    
    deinit {
//        DebugLog("DEALLOC")
        removeObserversUntilDeinit()
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserversUntilDisappear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserversUntilDisappear()
//        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Public
    
    func styleStatusBarBackground() {
        self.view.backgroundColor = UIColor.white
        self.view.insertSubview(bgView, at: 0)
        bgView.autoPinEdge(toSuperviewEdge: .top, withInset: SI.SCREEN_SIZE.STATUS_BAR_HEIGHT)
        bgView.autoPinEdge(toSuperviewEdge: .left)
        bgView.autoPinEdge(toSuperviewEdge: .right)
        bgView.autoPinEdge(toSuperviewEdge: .bottom)
    }
    
    func addTopFadeGradient() {
        let fadedView = SIGradientView.newAutoLayout()
        fadedView.colors = [
            UIColor.black.withAlphaComponent(0.2),
            UIColor.black.withAlphaComponent(0.0)
        ]
        self.view.addSubview(fadedView)
        fadedView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets.zero, excludingEdge: .bottom)
        fadedView.autoSetDimension(.height, toSize: 10.0)
    }
    
    // MARK: - Status Bar
    
    var statusBarHidden = false
    {
        didSet {
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.setNeedsStatusBarAppearanceUpdate()
            })
        }
    }
    
    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    
    override var prefersStatusBarHidden : Bool {
        return statusBarHidden
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
    }
    
    // MARK: - Observing
    
    func addObserversUntilDisappear() {
        if (!self.isObservingUntilDisappear) {
            self.isObservingUntilDisappear = true
        }
    }
    
    func removeObserversUntilDisappear() {
        if (self.isObservingUntilDisappear) {
            self.isObservingUntilDisappear = false
        }
    }
    
    func addObserversUntilDeinit() {
        if (!self.isObservingUntilDeinit) {
            self.isObservingUntilDeinit = true
        }
    }
    
    func removeObserversUntilDeinit() {
        if (self.isObservingUntilDeinit) {
            self.isObservingUntilDeinit = false
        }
    }
}
