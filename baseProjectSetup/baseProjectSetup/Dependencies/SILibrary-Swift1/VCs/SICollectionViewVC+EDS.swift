//
//  SICollectionViewVC+EDS.swift
//  PriortySMS
//
//  Created by Dan Hillman on 26/06/2017.
//  Copyright © 2017 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension SICollectionViewVC {
    
    // MARK: - SIProtocolEDS
    
    func titleForEmptyDataSet() -> String {
        return "Uh oh!"
    }
    
    func titleFontForEmptyDataSet() -> UIFont {
        return UIFont.boldSystemFont(ofSize: 17.0)
    }
    
    func titleColourForEmptyDataSet() -> UIColor {
        return UIColor.white
    }
    
    func descriptionForEmptyDataSet() -> String {
        return "You are seeing this screen due to a lack of data for display."
    }
    
    func descriptionFontForEmptyDataSet() -> UIFont {
        return UIFont.systemFont(ofSize: 14.0)
    }
    
    func descriptionColourForEmptyDataSet() -> UIColor {
        return UIColor.lightGray
    }
    
    func customViewForEmptyDataSet() -> UIView? {
        return nil
    }
    
    // MARK: - DZNEmptyDataSetSource
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return nil
    }
    
    func imageAnimation(forEmptyDataSet scrollView: UIScrollView!) -> CAAnimation! {
        let animation = CABasicAnimation(keyPath: "transform")
        animation.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(caTransform3D: CATransform3DMakeRotation(CGFloat(Double.pi / 2), 0.0, 0.0, 1.0))
        animation.duration = 0.25
        animation.isCumulative = true
        animation.repeatCount = MAXFLOAT
        return animation
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = edsDelegate.titleForEmptyDataSet()
        let attributes = [NSFontAttributeName: edsDelegate.titleFontForEmptyDataSet(), NSForegroundColorAttributeName: edsDelegate.titleColourForEmptyDataSet()]
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = edsDelegate.descriptionForEmptyDataSet()
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        let attributes = [NSFontAttributeName: edsDelegate.descriptionFontForEmptyDataSet(), NSForegroundColorAttributeName: edsDelegate.descriptionColourForEmptyDataSet(), NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return UIColor.darkGray
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return 0.0
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return 20.0
    }
    
    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        return edsDelegate.customViewForEmptyDataSet()
    }
    
    // MARK: DZNEmptyDataSetDelegate
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldAnimateImageView(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    //func emptyDataSetDidTapView(_ scrollView: UIScrollView!) {
    @objc(emptyDataSet:didTapView:) func emptyDataSet(_ scrollView: UIScrollView!, didTap didTapView:UIView!) {
        DebugLog("Tapped View")
        reloadData()
    }
    
    //func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
    @objc(emptyDataSet:didTapButton:) func emptyDataSet(_ scrollView: UIScrollView!, didTap didTapButton:UIButton!) {
        DebugLog("Tapped Button")
        reloadData()
    }
    
}
