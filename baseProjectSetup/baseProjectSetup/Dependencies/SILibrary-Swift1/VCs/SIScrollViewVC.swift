//
//  SIScrollViewVC.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 01/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import UIKit
import BlocksKit

class SIScrollViewVC: SIRootVC {
    
    // MARK: - Variables/Constants
    
    let scrollView  = SIScrollView.newAutoLayout()
    let contentView = UIView.newAutoLayout()
    
    var refreshControl: UIRefreshControl?
    
    // MARK: - Initialization
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    deinit {
        DebugLog("DEALLOC")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        super.loadView()
        
        setupScrollView()
        self.view.addSubview(scrollView)
        setupScrollViewConstraints()
        
        setupContentView()
        scrollView.addSubview(contentView)
        setupContentViewConstraints()
        
        self.view.setNeedsUpdateConstraints()
    }
    
    // MARK: - UI
    
    func setupScrollView() {
        scrollView.backgroundColor = UIColor.clear
        scrollView.delaysContentTouches = false
        scrollView.keyboardDismissMode = .interactive
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer().bk_init { [unowned self] (sender:UIGestureRecognizer?, state:UIGestureRecognizerState, location:CGPoint) in
            self.view.endEditing(true)
            } as! UITapGestureRecognizer
        self.scrollView.addGestureRecognizer(tapGesture)
    }
    
    func setupContentView() {
        contentView.backgroundColor = UIColor.clear
        
    }
    
    func setupScrollViewConstraints() {
        scrollView.autoPinEdgesToSuperviewEdges()
    }
    
    func setupContentViewConstraints() {
        contentView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets.zero)
        contentView.autoMatch(.width, to: .width, of: scrollView)
        contentView.autoConstrainAttribute(.leading,
                                           to: .leading,
                                           of: self.view,
                                           withMultiplier: 1,
                                           relation: .lessThanOrEqual)
        contentView.autoConstrainAttribute(.trailing,
                                           to: .trailing,
                                           of: self.view,
                                           withMultiplier: 1,
                                           relation: .lessThanOrEqual)
    }
    
    // MARK: - Public Refresh Control
    
    @discardableResult func addRefreshControl(_ title:String = "", backgroundColor:UIColor, tintColour:UIColor, selector:Selector) -> UIRefreshControl? {
        if ((refreshControl == nil)) {
            refreshControl = UIRefreshControl()
        }
        
        if let refreshControl = refreshControl {
            refreshControl.addTarget(self, action: selector, for: .valueChanged)
            refreshControl.backgroundColor = backgroundColor
            refreshControl.tintColor = tintColour
            refreshControl.attributedTitle = (NSAttributedString(string: title, attributes: [
                NSForegroundColorAttributeName : tintColour
                ]))
            scrollView.addSubview(refreshControl)
        }
        
        return refreshControl
    }
    
    func beginRefreshing() {
        UIView.animate(withDuration: 0.0,
                                   delay: 0.0,
                                   options: .beginFromCurrentState,
                                   animations: { [unowned self] in
                                    if let refreshControl = self.refreshControl {
                                        self.scrollView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.size.height)
                                    }
            }, completion: { [unowned self] (finished:Bool) in
                if let refreshControl = self.refreshControl {
                    refreshControl.beginRefreshing()
                }
            })
    }
    
    func endRefreshWithTimestampText(_ text:String, dateFormat:String = "MMM d, h:mm a", textColour:UIColor) {
        if let refreshControl = refreshControl {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            let title = text + dateFormatter.string(from: Date())
            let attributedTitle = NSAttributedString(string: title, attributes: [
                NSForegroundColorAttributeName : textColour
                ])
            refreshControl.attributedTitle = attributedTitle
            endRefreshing()
        }
    }
    
    func endRefreshing(_ scrollToTop:Bool = true) {
        scrollView.isScrollEnabled = false
        scrollView.isScrollEnabled = true
        if let refreshControl = refreshControl {
            refreshControl.endRefreshing()
            if scrollToTop {
                scrollView.setContentOffset(CGPoint.zero, animated: true)
            }
        }
    }
    
    // MARK: - Observing
    
    override func addObserversUntilDisappear() {
        if (!self.isObservingUntilDisappear) {
            NotificationCenter.default.addObserver(self,
                                                             selector: #selector(self.keyboardWillShowNotification),
                                                             name: NSNotification.Name.UIKeyboardWillShow,
                                                             object: nil)
            
            NotificationCenter.default.addObserver(self,
                                                             selector: #selector(self.keyboardWillHideNotification),
                                                             name: NSNotification.Name.UIKeyboardWillHide,
                                                             object: nil)
        }
        
        super.addObserversUntilDisappear()
    }
    
    override func removeObserversUntilDisappear() {
        if (self.isObservingUntilDisappear) {
            NotificationCenter.default.removeObserver(self,
                                                                name: NSNotification.Name.UIKeyboardWillShow,
                                                                object: nil)
            
            NotificationCenter.default.removeObserver(self,
                                                                name: NSNotification.Name.UIKeyboardWillHide,
                                                                object: nil)
        }
        
        super.removeObserversUntilDisappear()
    }
    
    // MARK: - Notifications
    
    func keyboardWillShowNotification(_ notification:Notification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                scrollView.contentInset = contentInsets
                scrollView.scrollIndicatorInsets = contentInsets
            }
        }
    }
    
    func keyboardWillHideNotification(_ notification:Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
}
