//
//  SICollectionViewFCVC.swift
//  PriortySMS
//
//  Created by Dan Hillman on 26/06/2017.
//  Copyright © 2017 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SICollectionViewFCVC: SICollectionViewVC, NSFetchedResultsControllerDelegate {
    
    // MARK: Variables/Constants
    
    var fetchedController: NSFetchedResultsController<NSFetchRequestResult>?
    
    // MARK: Initialization
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    deinit {
        DebugLog("DEALLOC")
        
        if let fc = fetchedController {
            fc.delegate = nil
        }
    }
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFetchedResultsControllerWithSearchString()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        super.loadView()
        
    }
    
    // MARK: Public
    
    func setupFetchedResultsControllerWithSearchString(_ searchString:String? = nil) {
        // override in child
    }
    
    func setFetchedResultsController(_ fc:NSFetchedResultsController<NSFetchRequestResult>) {
        fc.delegate = self
        do {
            try fc.performFetch()
        } catch let error {
            DebugLog("Unable to perform fetch: \(error.localizedDescription)")
        }
        fetchedController = fc
        reloadData()
    }
    
    func getTotalNumberOfObjects() -> Int {
        if let fc = fetchedController {
            if let fetchedObjects = fc.fetchedObjects {
                return fetchedObjects.count
            }
        }
        
        return 0
    }
    
    func getIndexPathForObject(_ object:NSManagedObject) -> IndexPath? {
        if let fc = fetchedController {
            if let fetchRequestEntityName = fc.fetchRequest.entityName {
                if let objectEntityName = object.entity.name {
                    if (fetchRequestEntityName == objectEntityName) {
                        return fc.indexPath(forObject: object)
                    }
                }
            }
        }
        
        return nil
    }
    
    func scrollToObject(_ object:NSManagedObject, scrollPosition:UICollectionViewScrollPosition, animated:Bool) {
        if let indexPath = getIndexPathForObject(object) {
            scrollToIndexPath(indexPath, scrollPosition: scrollPosition, animated: animated)
        }
    }
    
    // MARK: Overrides
    
    override func getNumSections() -> Int {
        if let fc = fetchedController {
            guard let sectionCount = fc.sections?.count else {
                return 0
            }
            return sectionCount
        } else {
            return super.getNumSections()
        }
    }
    
    override func getNumberOfRowsInSection(_ section: Int) -> Int {
        if let fc = fetchedController {
            guard let sectionData = fc.sections?[section] else {
                return 0
            }
            return sectionData.numberOfObjects
        } else {
            return super.getNumberOfRowsInSection(section)
        }
    }
    
    override func getObjectAtIndexPath(_ indexPath:IndexPath) -> Any? {
        if let fc = fetchedController {
            if let sections = fc.sections {
                if (indexPath.section < sections.count) {
                    let sectionInfo = sections[indexPath.section]
                    if let objects = sectionInfo.objects {
                        if (indexPath.row < objects.count) {
                            return objects[indexPath.row] as Any?
                        }
                    }
                }
            }
        }
        return super.getObjectAtIndexPath(indexPath)
    }
    
    // MARK: TableView Delegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    // MARK: -  FetchedResultsController Delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        //        if let tableView = tableView {
        //            tableView.beginUpdates()
        //        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        //        if let tableView = tableView {
        //            tableView.endUpdates()
        //        }
        
        reloadData()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        //        if let tableView = tableView {
        //            switch type {
        //            case .Insert:
        //                tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Automatic)
        //            case .Delete:
        //                tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Automatic)
        //            default: break
        //            }
        //        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        //        if let tableView = tableView {
        //            switch type {
        //            case .Insert:
        //                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
        //            case .Delete:
        //                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
        //            default: break
        //            }
        //        }
    }
    
}
