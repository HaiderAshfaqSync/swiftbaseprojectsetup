//
//  SITableViewVC.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 01/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class SITableViewVC: SIRootVC,
UITableViewDataSource, UITableViewDelegate,
DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, SIProtocolEDS {
    
    // MARK: Variables/Constants
    
    var tableView: SITableView?
    weak internal var edsDelegate: SIProtocolEDS!
    var refreshControl: UIRefreshControl?
    
    // MARK: Initialization
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    deinit {
        DebugLog("DEALLOC")
        
        if let tv = tableView {
            tv.dataSource = nil
            tv.delegate = nil
            edsDelegate = nil
            tv.emptyDataSetSource = nil
            tv.emptyDataSetDelegate = nil
        }
    }
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        super.loadView()
        
        setupTableView()
        if let tableView = tableView {
            self.view.addSubview(tableView)
            setupTableViewConstraints()
        }
        
        self.view.setNeedsUpdateConstraints()
    }
    
    // MARK: UI
    
    func setupTableView(_ style:UITableViewStyle = .plain) {
        tableView = SITableView(frame: CGRect.zero, style: style)
        if let tableView = tableView {
            tableView.translatesAutoresizingMaskIntoConstraints = false
            tableView.delegate = self
            tableView.dataSource = self
            edsDelegate = self
            tableView.emptyDataSetSource = self
            tableView.emptyDataSetDelegate = self
            tableView.separatorStyle = .none
            tableView.scrollsToTop = true
            tableView.delaysContentTouches = false
            tableView.keyboardDismissMode = .interactive
            tableView.backgroundColor = UIColor.clear
        }
    }
    
    func setupTableViewConstraints() {
        if let tableView = tableView {
            tableView.autoPinEdgesToSuperviewEdges()
        }
    }
    
    // MARK: - Public Helpers
    
    func scrollToIndexPath(_ indexPath:IndexPath, scrollPosition:UITableViewScrollPosition, animated:Bool) {
        if let tableView = tableView {
            tableView.scrollToRow(at: indexPath, at: scrollPosition, animated: animated)
        }
    }
    
    func scrollToTop(_ scrollPosition:UITableViewScrollPosition, animated:Bool) {
        if let tableView = tableView {
            if let firstIndexPath = tableView.getFirstIndexPath() {
                scrollToIndexPath(firstIndexPath as IndexPath, scrollPosition: scrollPosition, animated: animated)
            }
        }
    }
    
    func scrollToBottom(_ scrollPosition:UITableViewScrollPosition, animated:Bool) {
        if let tableView = tableView {
            if let lastIndexPath = tableView.getLastIndexPath() {
                scrollToIndexPath(lastIndexPath as IndexPath, scrollPosition: scrollPosition, animated: animated)
            }
        }
    }
    
    // MARK: - Public Data
    
    func getNumSections() -> Int {
        return 0
    }
    
    func getNumberOfRowsInSection(_ section: Int) -> Int {
        return 0
    }
    
    func getObjectAtIndexPath(_ indexPath:IndexPath) -> Any? {
        return nil
    }
    
    func getObjectAtFirstIndexPath() -> Any? {
        if let tableView = tableView {
            if let firstIndexPath = tableView.getFirstIndexPath() {
                return getObjectAtIndexPath(firstIndexPath as IndexPath)
            }
        }
        
        return nil
    }
    
    func getObjectAtLastIndexPath() -> Any? {
        if let tableView = tableView {
            if let firstIndexPath = tableView.getLastIndexPath() {
                return getObjectAtIndexPath(firstIndexPath as IndexPath)
            }
        }
        
        return nil
    }
    
    func reloadData() {
        if let tableView = tableView {
            tableView.reloadData()
        }
    }
    
    func reloadEmptyDataSet() {
        if let tableView = tableView {
            tableView.reloadEmptyDataSet()
        }
    }
    
    // MARK: - Public Refresh Control
    
    @discardableResult func addRefreshControl(_ title:String = "", backgroundColor:UIColor, tintColour:UIColor, selector:Selector) -> UIRefreshControl? {
        if ((refreshControl == nil)) {
            refreshControl = UIRefreshControl()
        }
        
        if let refreshControl = refreshControl {
            refreshControl.addTarget(self, action: selector, for: .valueChanged)
            refreshControl.backgroundColor = backgroundColor
            refreshControl.tintColor = tintColour
            refreshControl.attributedTitle = (NSAttributedString(string: title, attributes: [
            NSForegroundColorAttributeName : tintColour
            ]))
        }
        
        if let tableView = tableView {
            tableView.backgroundView = refreshControl
        }
        
        reloadData()
        return refreshControl
    }
    
    func beginRefreshing() {
        UIView.animate(withDuration: 0.0,
                                   delay: 0.0,
                                   options: .beginFromCurrentState,
                                   animations: { [unowned self] in
                                    if let refreshControl = self.refreshControl {
                                        if let tableView = self.tableView {
                                            tableView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.size.height)
                                        }
                                    }
            }, completion: { [unowned self] (finished:Bool) in
                if let refreshControl = self.refreshControl {
                    refreshControl.beginRefreshing()
                }
        })
    }
    
    func endRefreshWithTimestampText(_ text:String, dateFormat:String = "MMM d, h:mm a", textColour:UIColor) {
        if let refreshControl = refreshControl {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            let title = text + dateFormatter.string(from: Date())
            let attributedTitle = NSAttributedString(string: title, attributes: [
                NSForegroundColorAttributeName : textColour
                ])
            refreshControl.attributedTitle = attributedTitle
            endRefreshing()
        }
    }
    
    func endRefreshing() {
        if let refreshControl = refreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    // MARK: TableView Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getNumberOfRowsInSection(section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return getNumSections()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "SITableViewCell")
        if cell == nil {
            cell = SITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SITableViewCell")
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
        
        if let textLabel = cell.textLabel {
            textLabel.backgroundColor = UIColor.clear
        }
        
        if let detailTextLabel = cell.detailTextLabel {
            detailTextLabel.backgroundColor = UIColor.clear
        }
    }
    
    // MARK: TableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            scrollView.contentOffset = CGPoint.zero
        }
    }
    
    // MARK: Observing
    
    override func addObserversUntilDisappear() {
        if (!self.isObservingUntilDisappear) {
            NotificationCenter.default.addObserver(self,
                                                             selector: #selector(self.keyboardWillShowNotification),
                                                             name: NSNotification.Name.UIKeyboardWillShow,
                                                             object: nil)
            
            NotificationCenter.default.addObserver(self,
                                                             selector: #selector(self.keyboardWillHideNotification),
                                                             name: NSNotification.Name.UIKeyboardWillHide,
                                                             object: nil)
        }
        
        super.addObserversUntilDisappear()
    }
    
    override func removeObserversUntilDisappear() {
        if (self.isObservingUntilDisappear) {
            NotificationCenter.default.removeObserver(self,
                                                                name: NSNotification.Name.UIKeyboardWillShow,
                                                                object: nil)
            
            NotificationCenter.default.removeObserver(self,
                                                                name: NSNotification.Name.UIKeyboardWillHide,
                                                                object: nil)
        }
        
        super.removeObserversUntilDisappear()
    }
    
    // MARK: NOTIFICATIONS
    
    func keyboardWillShowNotification(_ notification:Notification) {
        if let tableView = tableView {
            if let userInfo = notification.userInfo {
                if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                    let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                    tableView.contentInset = contentInsets
                    tableView.scrollIndicatorInsets = contentInsets
                }
            }
        }
    }
    
    func keyboardWillHideNotification(_ notification:Notification) {
        if let tableView = tableView {
            let contentInsets = UIEdgeInsets.zero
            tableView.contentInset = contentInsets
            tableView.scrollIndicatorInsets = contentInsets
        }
    }
    
}
