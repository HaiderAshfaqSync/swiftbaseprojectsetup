//
//  UITextField+SIExtras.swift
//  SILibrary-Swift
//
//  Created by Dan Hillman on 22/12/2016.
//  Copyright © 2016 Dan Hillman. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func stylePlaceholder(placeholder:String?, textColour:UIColor) {
        if let placeholder = placeholder {
            self.attributedPlaceholder = NSAttributedString(string:placeholder,
                                                            attributes:[NSForegroundColorAttributeName:textColour])
        } else {
            self.attributedPlaceholder = NSAttributedString(string:"",
                                                            attributes:[NSForegroundColorAttributeName:textColour])
        }
    }
    
    /// EZSE: Add left padding to the text in textfield
    public func addLeftTextPadding(_ blankSize: CGFloat) {
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: blankSize, height: frame.height)
        self.leftView = leftView
        self.leftViewMode = UITextFieldViewMode.always
    }
    
}
