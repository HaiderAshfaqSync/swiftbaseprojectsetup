//
//  NSUserDefaults+SIExtras.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 01/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    class func setObject(_ object:Any?, key:String) {
        let userDefaults = UserDefaults.standard
        userDefaults .set(object, forKey: key)
        userDefaults.synchronize()
    }
    
    class func objectForKey(_ key:String) -> Any? {
        return UserDefaults.standard.object(forKey: key) as Any?
    }
    
}

