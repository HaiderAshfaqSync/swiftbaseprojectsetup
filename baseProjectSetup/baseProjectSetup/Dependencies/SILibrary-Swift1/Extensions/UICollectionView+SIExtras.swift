//
//  UICollectionView+SIExtras.swift
//  MozaicCollectionViewTest
//
//  Created by Haider Ashfaq on 29/11/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    
    func getFirstIndexPath() -> IndexPath? {
        let firstSectionIndex = 0
        if (firstSectionIndex < numberOfSections) {
            return getFirstRowForSection(firstSectionIndex)
        }
        return nil
    }
    
    func getFirstRowForSection(_ firstSectionIndex:Int) -> IndexPath? {
        if (firstSectionIndex < numberOfSections) {
            if (numberOfItems(inSection: firstSectionIndex) > 0) {
                let lastIndexPath = IndexPath(row:0, section: firstSectionIndex)
                return lastIndexPath
            } else {
                return getFirstRowForSection(firstSectionIndex + 1)
            }
        }
        
        return nil
    }
    
    func getLastIndexPath() -> IndexPath? {
        let lastSectionIndex = numberOfSections - 1
        if (lastSectionIndex >= 0) {
            return getLastRowForSection(lastSectionIndex)
        }
        return nil
    }
    
    func getLastRowForSection(_ lastSectionIndex:Int) -> IndexPath? {
        let lastSectionLastRow = numberOfItems(inSection: lastSectionIndex) - 1
        if (lastSectionIndex >= 0) {
            if (lastSectionLastRow >= 0) {
                let lastIndexPath = IndexPath(row:lastSectionLastRow, section: lastSectionIndex)
                return lastIndexPath
            } else {
                return getLastRowForSection(lastSectionIndex - 1)
            }
        }
        
        return nil
    }
    
}

