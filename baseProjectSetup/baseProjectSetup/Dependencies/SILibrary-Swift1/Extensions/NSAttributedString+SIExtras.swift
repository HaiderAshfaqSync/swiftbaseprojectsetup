//
//  NSAttributedString+SIExtras.swift
//  SILibrary-Swift
//
//  Created by Dan Hillman on 22/12/2016.
//  Copyright © 2016 Dan Hillman. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
    
    func getAttributedString(
        adjustText:String?,
        font:UIFont,
        textColour:UIColor
        ) -> NSAttributedString? {
        
        let attributedString = NSMutableAttributedString(attributedString: self)
        if let adjustText = adjustText {
            let boldFontAttribute = [
                NSFontAttributeName: font,
                NSForegroundColorAttributeName:textColour
            ]
            let range = NSString(string: attributedString.string).range(of: adjustText)
            if range.location != NSNotFound {
                attributedString.addAttributes(boldFontAttribute, range: range)
                return attributedString
            }
        }
        return nil
    }
    
    func underline() -> NSAttributedString {
        guard let copy = self.mutableCopy() as? NSMutableAttributedString else { return self }
        let range = (self.string as NSString).range(of: self.string)
        copy.addAttributes([NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue], range: range)
        return copy
    }
    
    func italic() -> NSAttributedString {
        guard let copy = self.mutableCopy() as? NSMutableAttributedString else { return self }
        let range = (self.string as NSString).range(of: self.string)
        copy.addAttributes([NSFontAttributeName: UIFont.italicSystemFont(ofSize: UIFont.systemFontSize)], range: range)
        return copy
    }
    
    func strikethrough() -> NSAttributedString {
        guard let copy = self.mutableCopy() as? NSMutableAttributedString else { return self }
        let range = (self.string as NSString).range(of: self.string)
        let attributes = [
            NSStrikethroughStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue as Int)]
        copy.addAttributes(attributes, range: range)
        return copy
    }
    
    func color(_ color: UIColor) -> NSAttributedString {
        guard let copy = self.mutableCopy() as? NSMutableAttributedString else { return self }
        let range = (self.string as NSString).range(of: self.string)
        copy.addAttributes([NSForegroundColorAttributeName: color], range: range)
        return copy
    }
    
    /// SwifterSwift: Add a NSAttributedString to another NSAttributedString
    ///
    /// - Parameters:
    ///   - lhs: NSAttributedString to add to.
    ///   - rhs: NSAttributedString to add.
    public static func += (lhs: inout NSAttributedString, rhs: NSAttributedString) {
        let ns = NSMutableAttributedString(attributedString: lhs)
        ns.append(rhs)
        lhs = ns
    }
    
}
