//
//  UIWindow+SIExtras.swift
//  SILibrary-Swift
//
//  Created by Dan Hillman on 22/12/2016.
//  Copyright © 2016 Dan Hillman. All rights reserved.
//

import Foundation
import UIKit

private let blurViewtag = 198489

extension UIWindow {
    
    // MARK: - Blurring
    
    /*
    // Usage: Add in AppDelegate
     
    func applicationWillResignActive(_ application: UIApplication) {
        window.blurPresentedView()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        window.unblurPresentedView()
    }
    */
    
    func blurPresentedView() {
        if let _ = self.viewWithTag(blurViewtag) {
            return
        }
        
        let snapshot = bluredSnapshot()
        self.addSubview(snapshot!)
    }
    
    func unblurPresentedView() {
        if let blurView = self.viewWithTag(blurViewtag) {
            blurView.removeFromSuperview()
        }
    }
    
    func bluredSnapshot () -> UIView? {
        if let snapshot = self.snapshotView(afterScreenUpdates: true) {
            snapshot.tag = blurViewtag
            snapshot.addSubview(blurView(frame: (snapshot.frame)))
            return snapshot
        }
        return nil
    }
    
    func blurView(frame: CGRect)->UIView {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        view.frame = frame
        return view
    }
    
}
