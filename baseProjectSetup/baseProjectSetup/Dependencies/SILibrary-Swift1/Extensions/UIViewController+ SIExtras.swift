//
//  UIViewController+App.swift
//  LipSuite
//
//  Created by Dan H (Sync Interactive) on 04/05/2016.
//  Copyright © 2016 Sync Interactive Ltd. All rights reserved.
//

import Foundation
import UIKit
import BlocksKit

extension UIViewController {
    
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!;
    }
    
    func isModal() -> Bool {
        if self.presentingViewController != nil {
            return true
        } else if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        } else if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
    
    func getNavControllerNumVCs() -> Int {
        if let nc = self.navigationController {
            return nc.viewControllers.count
        }
        
        return 0
    }
    
    func isModalAndShouldPop() -> Bool {
        return (isModal() && getNavControllerNumVCs() > 1)
    }
    
    func isModalAndShouldDismiss() -> Bool {
        return (isModal() && getNavControllerNumVCs() <= 1)
    }
    
    func alert(_ title: String?, message:String?, buttonTitle:String?) {
        alert(title, message: message, buttonTitle: buttonTitle) { (UIAlertAction) in
            DebugLog("OK")
        }
    }
    
    func alert(_ title: String?, message:String?, buttonTitle:String?, handler: ((UIAlertAction) -> Void)?) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let button: UIAlertAction = UIAlertAction(title:buttonTitle, style: .default, handler: handler)
        alertController.addAction(button)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentMailComposerWithSubject(
        _ subject:String,
        body:String,
        toRecipients:[String]? = nil,
        isBodyHtml:Bool = true,
        animated:Bool = true,
        addAttachmentsBlock:((MFMailComposeViewController?) -> Void)? = nil,
        styleComposerBlock:((MFMailComposeViewController?) -> Void)? = nil,
        presentedBlock: (() -> Void)? = nil,
        dismissedBlock: (() -> Void)? = nil,
        completionBlock:@escaping ((MFMailComposeViewController?, MFMailComposeResult, Error?) -> Void),
        cannotSendMailBlock: (() -> Void)
        )
    {
        if MFMailComposeViewController.canSendMail() {
            let vc = MFMailComposeViewController()

            vc.bk_completionBlock = { [weak self] (controller, result, error) -> Void in
                completionBlock(controller, result, error)
                if let weakSelf = self {
                    weakSelf.dismiss(animated: animated, completion: dismissedBlock)
                }
            }
            
            vc.setToRecipients(toRecipients)
            vc.setSubject(subject)
            vc.setMessageBody(body, isHTML: isBodyHtml)
            
            if let addAttachmentsBlock = addAttachmentsBlock {
                addAttachmentsBlock(vc)
            }
            
            if let styleComposerBlock = styleComposerBlock {
                styleComposerBlock(vc)
            }
            
            self.present(vc,
                                       animated: animated,
                                       completion: presentedBlock)
            
        } else {
            cannotSendMailBlock()
        }
    }
    
    // MARK: - Notifications
    
    ///EZSE: Adds an NotificationCenter with name and Selector
    public func addNotificationObserver(_ name: String, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name(rawValue: name), object: nil)
    }
    
    ///EZSE: Removes an NSNotificationCenter for name
    public func removeNotificationObserver(_ name: String) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: name), object: nil)
    }
    
    ///EZSE: Removes NotificationCenter'd observer
    public func removeNotificationObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - VC Container
    
    ///EZSE: Returns Tab Bar's height
    public var tabBarHeight: CGFloat {
        get {
            if let me = self as? UINavigationController, let visibleViewController = me.visibleViewController {
                return visibleViewController.tabBarHeight
            }
            if let tab = self.tabBarController {
                return tab.tabBar.frame.size.height
            }
            return 0
        }
    }
    
    ///EZSE: Returns Navigation Bar's height
    public var navigationBarHeight: CGFloat {
        get {
            if let me = self as? UINavigationController, let visibleViewController = me.visibleViewController {
                return visibleViewController.navigationBarHeight
            }
            if let nav = self.navigationController {
                return nav.navigationBar.height
            }
            return 0
        }
    }
    
    ///EZSE: Returns Navigation Bar's color
    public var navigationBarColor: UIColor? {
        get {
            if let me = self as? UINavigationController, let visibleViewController = me.visibleViewController {
                return visibleViewController.navigationBarColor
            }
            return navigationController?.navigationBar.tintColor
        } set(value) {
            navigationController?.navigationBar.barTintColor = value
        }
    }
    
    ///EZSE: Returns current Navigation Bar
    public var navBar: UINavigationBar? {
        get {
            return navigationController?.navigationBar
        }
    }
    
    ///EZSE: Adds the specified view controller as a child of the current view controller.
    public func addAsChildViewController(_ vc: UIViewController, toView: UIView) {
        self.addChildViewController(vc)
        toView.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
}
