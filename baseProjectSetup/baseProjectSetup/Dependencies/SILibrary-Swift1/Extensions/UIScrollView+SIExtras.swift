//
//  UIScrollView+SIExtras.swift
//  BristolCityFCPrioritySMS
//
//  Created by Haider Ashfaq on 23/02/2018.
//  Copyright © 2018 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit

extension UIScrollView {
    /// Sets content offset to the top.
    func resetScrollPositionToTop() {
        self.contentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
    }
}
