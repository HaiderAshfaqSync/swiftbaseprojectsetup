//
//  UIFont+App.swift
//  LipSuite
//
//  Created by Abdullah Awan on 22/04/2016.
//  Copyright © 2016 Sync Interactive Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    class func logAllFontNames() {
        for family in UIFont.familyNames {
            SilentLog("\(family)")
            
            for name in UIFont.fontNames(forFamilyName: family) {
                SilentLog("   \(name)")
            }
        }
    }
    
}
