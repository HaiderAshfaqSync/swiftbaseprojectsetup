//
//  UILabel+SIExtras.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 03/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func setMinimumFontSize_si(_ size:CGFloat) {
        self.minimumScaleFactor = size / self.font.pointSize
        self.adjustsFontSizeToFitWidth = true
    }
    
    func underlineWithText(text:String?) {
        if let text = text {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = self.textAlignment
            let attributedString = NSMutableAttributedString(
                string: text,
                attributes: [
                    NSFontAttributeName:self.font,
                    NSForegroundColorAttributeName:self.textColor,
                    NSParagraphStyleAttributeName:paragraphStyle
                ]
            )
            self.attributedText = attributedString.underline()
        } else {
            self.attributedText = nil
        }
    }
    
    // Call this when setting a label text for the first time
    func setTextWithAdjustmentStyle(
        _ text:String?,
        adjustText:String?,
        font:UIFont,
        textColour:UIColor
        ) {
        
        if let text = text {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = self.textAlignment
            let attributedString = NSMutableAttributedString(
                string: text,
                attributes: [
                    NSFontAttributeName:self.font,
                    NSForegroundColorAttributeName:self.textColor,
                    NSParagraphStyleAttributeName:paragraphStyle
                ]
            )
            
            self.attributedText = attributedString
            
            adjustmentStyle(
                adjustText,
                font: font,
                textColour: textColour,
                paragraphStyle: paragraphStyle
            )
        }
    }
    
    // Call this when adjusting an already existing style of NSAttributedText
    func adjustmentStyle(
        _ adjustText:String?,
        font:UIFont,
        textColour:UIColor,
        paragraphStyle:NSMutableParagraphStyle
        ) {
        
        if let attributedText = attributedText {
            let attributedString = NSMutableAttributedString(attributedString: attributedText)
            if let adjustText = adjustText {
                let boldFontAttribute = [
                    NSFontAttributeName: font,
                    NSForegroundColorAttributeName:textColour,
                    NSParagraphStyleAttributeName:paragraphStyle
                ]
                let range = NSString(string: attributedString.string).range(of: adjustText)
                if range.location != NSNotFound {
                    attributedString.addAttributes(boldFontAttribute, range: range)
                    self.attributedText = attributedString
                }
            }
        }
    }
    
}
