//
//  Array+SIExtras.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 08/08/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    @discardableResult mutating func removeObject<U: Equatable>(_ object: U) -> Bool {
        for (idx, objectToCompare) in self.enumerated() {  //in old swift use enumerate(self)
            if let to = objectToCompare as? U {
                if object == to {
                    self.remove(at: idx)
                    return true
                }
            }
        }
        return false
    }
}
