//
//  UINavigationController+SIExtras.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 02/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    func replaceCurrentVCWith(vc:UIViewController, animated:Bool) {
        let navStack = NSMutableArray(array: self.viewControllers)
        navStack.removeLastObject()
        navStack.add(vc)
        let swiftArray = navStack as NSArray as! [UIViewController]
        self.setViewControllers(swiftArray, animated: animated)
    }
    
    func clearStackVCWith(stack:[UIViewController], keepRootVC:Bool, animated:Bool) {
        let navStack = NSMutableArray(array: self.viewControllers)
        
        var vc:Any?
        if keepRootVC {
            if navStack.count > 0 {
                vc = navStack.firstObject
            }
        }
        
        navStack.removeAllObjects()
        
        if keepRootVC {
            if let vc = vc {
                navStack.add(vc)
            }
        }
        
        navStack.addObjects(from: stack)
        let swiftArray = navStack as NSArray as! [UIViewController]
        self.setViewControllers(swiftArray, animated: animated)
    }
    
    func getNavigationBarHeight() -> CGFloat {
        return navigationBar.frame.height
    }
    
}
