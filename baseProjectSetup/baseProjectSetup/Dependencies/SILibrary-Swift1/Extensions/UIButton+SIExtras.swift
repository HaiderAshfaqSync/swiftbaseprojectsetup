//
//  UIButton+SIExtras.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 03/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//  Modified by Abdullah Awan 01/12/2016

import Foundation
import UIKit
import BlocksKit

extension UIButton {
    
    func setAlignImageRight() {
        self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        
        if let titleLabel = self.titleLabel {
            titleLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        
        if let imageView = self.imageView {
            imageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }
    
    func setAlignImageTop(_ sideEdgeInsets:UIEdgeInsets = UIEdgeInsets.zero, spacing: CGFloat = 0.0) {
        if let imageView = imageView {
            if let image = imageView.image {
                if let titleLabel = titleLabel {
                    if let text = titleLabel.text {
                        let imageSize: CGSize = image.size
                        titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
                        
                        let labelString = NSString(string: text)
                        let titleSize = labelString.size(attributes: [NSFontAttributeName: titleLabel.font])
                        imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
                        
                        let edgeOffset = abs(titleSize.height + spacing) / 2.0
                        contentEdgeInsets = UIEdgeInsetsMake(
                            sideEdgeInsets.top + edgeOffset,
                            sideEdgeInsets.left,
                            sideEdgeInsets.bottom + edgeOffset,
                            sideEdgeInsets.right
                        )
                        
//                        imageView.debugView()
//                        titleLabel.debugView()
                        
                    }
                }
            }
        }
    }
    
    public func setBackgroundColor(_ color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()?.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    func bk_replaceEventHandler(
        _ handler: ((Any?) -> Swift.Void)?,
        for controlEvents: UIControlEvents
        ) {
        
        self.bk_removeEventHandlers(for: controlEvents)
        if let handler = handler {
            self.bk_addEventHandler(handler, for: controlEvents)
        }
    }
    
    func styleAttributedTitle(_ attrs:Dictionary<String,Any>, state: UIControlState) {
        if let attributedString = self.attributedTitle(for: state) {
            let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
            let textRange = NSMakeRange(0, mutableAttributedString.mutableString.length)
            mutableAttributedString.setAttributes(attrs, range: textRange)
            self.setAttributedTitle(mutableAttributedString, for:state)
        }
    }
    
    func styleEnabled(_ enabled:Bool, alphaEnabledOff:CGFloat = 0.4) {
        self.isEnabled = enabled
        self.alpha = enabled ? 1.0 : alphaEnabledOff
    }
    
}
