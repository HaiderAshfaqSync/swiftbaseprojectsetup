//
//  SIRootVC+SIEctras.swift
//  BristolCityFCPrioritySMS
//
//  Created by Haider Ashfaq on 10/04/2018.
//  Copyright © 2018 Haider Ashfaq. All rights reserved.
//

import Foundation

extension SIRootVC {
    func createCutShape(){
        let layerView: UIView = {
            let lView = UIView.newAutoLayout()
            return lView
        }()
        
        self.view.addSubview(layerView)
        
        let layer = CAShapeLayer()
        
        var point3:CGFloat = 0
        if let frameTop = self.navigationController?.navigationBar.frame.minY {
            point3 = frameTop * 0.1
        }
        let myBezier = UIBezierPath()
        myBezier.move(to: CGPoint(x: 0, y: 0))
        myBezier.addLine(to: CGPoint(x: self.view.frame.width, y: 0))
        myBezier.addLine(to: CGPoint(x: self.view.frame.width, y: point3))
        myBezier.addLine(to: CGPoint(x: 0, y: point3 + 10))
        
        myBezier.close()
        
        layer.path = myBezier.cgPath
        layer.fillColor = UIColor.blue.cgColor
        layer.strokeColor = nil
        
        layerView.layer.addSublayer(layer)
    }
}
