//
//  Dictionary+SIExtras.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 08/08/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation

extension Dictionary {
    
    mutating func append(_ other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
    
    /// EZSE: Returns a random element inside Dictionary
    public func random() -> Value {
        let index: Int = Int(arc4random_uniform(UInt32(self.count)))
        return Array(self.values)[index]
    }
    
    /// EZSE: Checks if a key exists in the dictionary.
    public func has(_ key: Key) -> Bool {
        return index(forKey: key) != nil
    }
    
}
