//
//  NSMutableAttributedString+SIExtras.swift
//  SILibrary-Swift
//
//  Created by Dan Hillman on 22/12/2016.
//  Copyright © 2016 Dan Hillman. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    
    func setAsLink(textToFind:String, linkURL:String) -> Bool {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSLinkAttributeName, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
    
}
