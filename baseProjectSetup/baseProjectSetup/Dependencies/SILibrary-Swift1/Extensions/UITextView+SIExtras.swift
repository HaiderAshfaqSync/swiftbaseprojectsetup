//
//  UITextView+SIExtras.swift
//  SILibrary-Swift
//
//  Created by Dan Hillman on 22/12/2016.
//  Copyright © 2016 Dan Hillman. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    
    /// EZSE: Automatically adds a toolbar with a done button to the top of the keyboard. Tapping the button will dismiss the keyboard.
    public func addDoneButton(_ barStyle: UIBarStyle = .default, title: String? = nil) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: title ?? "Done", style: .done, target: self, action: #selector(resignFirstResponder))
        ]
        keyboardToolbar.barStyle = barStyle
        keyboardToolbar.sizeToFit()
        inputAccessoryView = keyboardToolbar
    }
    
}
