//
//  UIImage+SIExtras.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 02/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    /// EZSE: Returns compressed image to rate from 0 to 1
    public func compressImage(rate: CGFloat) -> Data? {
        return UIImageJPEGRepresentation(self, rate)
    }
    
    /// EZSE: Returns Image size in Bytes
    public func getSizeAsBytes() -> Int {
        return UIImageJPEGRepresentation(self, 1)?.count ?? 0
    }
    
    /// EZSE: Returns Image size in Kylobites
    public func getSizeAsKilobytes() -> Int {
        let sizeAsBytes = getSizeAsBytes()
        return sizeAsBytes != 0 ? sizeAsBytes / 1024 : 0
    }
    
    /// EZSE:
    public func aspectHeightForWidth(_ width: CGFloat) -> CGFloat {
        return (width * self.size.height) / self.size.width
    }
    
    /// EZSE:
    public func aspectWidthForHeight(_ height: CGFloat) -> CGFloat {
        return (height * self.size.width) / self.size.height
    }
    
    // MARK: - Colour
    
    class func imageWithColor(_ color:UIColor) -> UIImage {
        let rect:CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context:CGContext = UIGraphicsGetCurrentContext()!
        context.setFillColor(color.cgColor)
        context.fill(rect)
        let img:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return img
    }
    
    func tintWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
    // MARK: - Scaling
    
    func scaleImageToMaxDimension(_ maxDimension: CGFloat) -> UIImage {
        var scaledSize = CGSize(width: maxDimension, height: maxDimension);
        var scaleFactor: CGFloat
        
        let width = self.size.width
        let height = self.size.height
        
        if width > height {
            scaleFactor = height / width
            scaledSize.width = maxDimension
            scaledSize.height = scaledSize.height * scaleFactor
        }
        else {
            scaleFactor = width / height
            scaledSize.height = maxDimension
            scaledSize.width = scaledSize.height * scaleFactor
        }
        
        UIGraphicsBeginImageContext(scaledSize)
        self.draw(in: CGRect(x: 0, y: 0, width: scaledSize.width, height: scaledSize.height))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
    
    func scaleImageToSize(newSize: CGSize) -> UIImage? {
        var scaledImageRect = CGRect.zero
        
        let aspectWidth = newSize.width/size.width
        let aspectheight = newSize.height/size.height
        
        let aspectRatio = max(aspectWidth, aspectheight)
        
        scaledImageRect.size.width = size.width * aspectRatio;
        scaledImageRect.size.height = size.height * aspectRatio;
        scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0;
        scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0;
        
        UIGraphicsBeginImageContext(newSize)
        draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    /// EZSE Returns resized image with width. Might return low quality
    public func resizeWithWidth(_ width: CGFloat) -> UIImage {
        let aspectSize = CGSize (width: width, height: aspectHeightForWidth(width))
        
        UIGraphicsBeginImageContext(aspectSize)
        self.draw(in: CGRect(origin: CGPoint.zero, size: aspectSize))
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img!
    }
    
    /// EZSE Returns resized image with height. Might return low quality
    public func resizeWithHeight(_ height: CGFloat) -> UIImage {
        let aspectSize = CGSize (width: aspectWidthForHeight(height), height: height)
        
        UIGraphicsBeginImageContext(aspectSize)
        self.draw(in: CGRect(origin: CGPoint.zero, size: aspectSize))
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img!
    }
    
    public func croppedImage(_ bound: CGRect) -> UIImage? {
        guard self.size.width > bound.origin.x else {
            print("EZSE: Your cropping X coordinate is larger than the image width")
            return nil
        }
        guard self.size.height > bound.origin.y else {
            print("EZSE: Your cropping Y coordinate is larger than the image height")
            return nil
        }
        let scaledBounds: CGRect = CGRect(x: bound.origin.x * self.scale, y: bound.origin.y * self.scale, width: bound.size.width * self.scale, height: bound.size.height * self.scale)
        let imageRef = self.cgImage?.cropping(to: scaledBounds)
        let croppedImage: UIImage = UIImage(cgImage: imageRef!, scale: self.scale, orientation: UIImageOrientation.up)
        return croppedImage
    }
    
    // MARK: - UIView
    
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
//        self.init(cgImage: image!.cgImage!, scale: SI.SCREEN_SIZE.SCREEN_SCALE, orientation: UIImageOrientation.up)
    }
    
    // MARK: - Fix orientation of an UIImage (Sets orientation to portrait)
    
    func fixedOrientation() -> UIImage {
        
        if imageOrientation == UIImageOrientation.up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat(-(Double.pi / 2)))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil,
                                       width: Int(size.width),
                                       height: Int(size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent,
                                       bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
    
    // MARK: - UIImage Full Dimension
    
    /*
     Universal
     key = "ad1"
     ad1.png
     ad1@2x.png
     ad1@3x.png
     */
    class func getUniversalImageWithPrefixKey(_ key:String, returnNilImage:Bool = false) -> (image:UIImage?, fileName:String) {
        if (SI.SCREEN_SIZE.SCREEN_SCALE == 3.0) {
            if returnNilImage {
                let filename = "\(key)@3x"
                return (nil, filename)
            }
        }
        
        if (SI.SCREEN_SIZE.SCREEN_SCALE == 2.0) {
            if returnNilImage {
                let filename = "\(key)@2x"
                return (nil, filename)
            }
        }
        
        let filename = "\(key)"
        if returnNilImage {
            return (nil, filename)
        } else {
            let image = UIImage(named: filename)
            return (image, filename)
        }
    }
    
    /*
     PORTRAIT WIDTH
     key = "ad1"
     ad1-320w.png
     ad1-320w@2x.png
     ad1-375w@2x.png
     ad1-414w@3x.png
     */
    class func getFullPortraitWidthImageWithPrefixKey(_ key:String, returnNilImage:Bool = false) -> (image:UIImage?, fileName:String) {
        DebugLog("Screen Width: \(SI.SCREEN_SIZE.SCREEN_WIDTH)")
        DebugLog("Scale: \(SI.SCREEN_SIZE.SCREEN_SCALE)")
        
        if (SI.SCREEN_SIZE.SCREEN_WIDTH >= 414 && SI.SCREEN_SIZE.SCREEN_SCALE == 3.0) {
            var filename = "\(key)-414w"
            if returnNilImage {
                filename = "\(filename)@3x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        if (SI.SCREEN_SIZE.SCREEN_WIDTH >= 375 && SI.SCREEN_SIZE.SCREEN_SCALE == 2.0) {
            var filename = "\(key)-375w"
            if returnNilImage {
                filename = "\(filename)@2x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        let filename = "\(key)-320w"
        if returnNilImage {
            return (nil, filename)
        } else {
            let image = UIImage(named: filename)
            return (image, filename)
        }
    }
    
    
    /*
     LANDSCAPE HEIGHT
     key = "ad1"
     ad1-320h.png
     ad1-320h@2x.png
     ad1-375h@2x.png
     ad1-414h@3x.png
     */
    class func getFullLandscapeHeightImageWithPrefixKey(_ key:String, returnNilImage:Bool = false) -> (image:UIImage?, fileName:String) {
        DebugLog("Screen Height: \(SI.SCREEN_SIZE.SCREEN_HEIGHT)")
        DebugLog("Scale: \(SI.SCREEN_SIZE.SCREEN_SCALE)")
        
        if (SI.SCREEN_SIZE.SCREEN_HEIGHT >= 414 && SI.SCREEN_SIZE.SCREEN_SCALE == 3.0) {
            var filename = "\(key)-414h"
            if returnNilImage {
                filename = "\(filename)@3x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        if (SI.SCREEN_SIZE.SCREEN_HEIGHT >= 375 && SI.SCREEN_SIZE.SCREEN_SCALE == 2.0) {
            var filename = "\(key)-375h"
            if returnNilImage {
                filename = "\(filename)@2x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        let filename = "\(key)-320h"
        if returnNilImage {
            return (nil, filename)
        } else {
            let image = UIImage(named: filename)
            return (image, filename)
        }
    }
    
    /*
     PORTRAIT HEIGHT *OR* PORTRAIT HEIGHT AND WIDTH
     key = "ad1"
     ad1-480h.png    iPhone 4/4s
     ad1-480h@2x.png iPhone 4/4s
     ad1-568h@2x.png iPhone 5/5s
     ad1-667h@2x.png iPhone 6
     ad1-736h@3x.png iPhone 6Plus
     */
    class func getFullPortraitHeightImageWithPrefixKey(_ key:String, returnNilImage:Bool = false) -> (image:UIImage?, fileName:String) {
        DebugLog("Screen Height: \(SI.SCREEN_SIZE.SCREEN_HEIGHT)")
        DebugLog("Scale: \(SI.SCREEN_SIZE.SCREEN_SCALE)")
        
        if (SI.SCREEN_SIZE.SCREEN_HEIGHT >= 736 && SI.SCREEN_SIZE.SCREEN_SCALE == 3.0) {
            var filename = "\(key)-736h"
            if returnNilImage {
                filename = "\(filename)@3x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        if (SI.SCREEN_SIZE.SCREEN_HEIGHT >= 667 && SI.SCREEN_SIZE.SCREEN_SCALE == 2.0) {
            var filename = "\(key)-667h"
            if returnNilImage {
                filename = "\(filename)@2x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        if (SI.SCREEN_SIZE.SCREEN_HEIGHT >= 568 && SI.SCREEN_SIZE.SCREEN_SCALE == 2.0) {
            var filename = "\(key)-568h"
            if returnNilImage {
                filename = "\(filename)@2x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        let filename = "\(key)-480h"
        if returnNilImage {
            return (nil, filename)
        } else {
            let image = UIImage(named: filename)
            return (image, filename)
        }
    }
    
    /*
     LANDSCAPE WIDTH *OR* LANDSCAPE WIDTH AND HEIGHT
     key = "ad1"
     ad1-480w.png    iPhone 4/4s
     ad1-480w@2x.png iPhone 4/4s
     ad1-568w@2x.png iPhone 5/5s
     ad1-667w@2x.png iPhone 6
     ad1-736w@3x.png iPhone 6Plus
     */
    class func getFullLandscapeWidthImageWithPrefixKey(_ key:String, returnNilImage:Bool = false) -> (image:UIImage?, fileName:String) {
        DebugLog("Screen Width: \(SI.SCREEN_SIZE.SCREEN_WIDTH)")
        DebugLog("Scale: \(SI.SCREEN_SIZE.SCREEN_SCALE)")
        
        if (SI.SCREEN_SIZE.SCREEN_WIDTH >= 736 && SI.SCREEN_SIZE.SCREEN_SCALE == 3.0) {
            var filename = "\(key)-736w"
            if returnNilImage {
                filename = "\(filename)@3x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        if (SI.SCREEN_SIZE.SCREEN_WIDTH >= 667 && SI.SCREEN_SIZE.SCREEN_SCALE == 2.0) {
            var filename = "\(key)-667w"
            if returnNilImage {
                filename = "\(filename)@2x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        if (SI.SCREEN_SIZE.SCREEN_WIDTH >= 568 && SI.SCREEN_SIZE.SCREEN_SCALE == 2.0) {
            var filename = "\(key)-568w"
            if returnNilImage {
                filename = "\(filename)@2x"
                return (nil, filename)
            } else {
                if let image = UIImage(named: filename) {
                    return (image, filename)
                }
            }
        }
        
        let filename = "\(key)-480w"
        if returnNilImage {
            return (nil, filename)
        } else {
            let image = UIImage(named: filename)
            return (image, filename)
        }
    }
    
}

