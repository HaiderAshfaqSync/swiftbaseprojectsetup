//
//  Double+SIExtras.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 08/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    
    var toFloat:CGFloat   {return CGFloat(self)}
    
    func roundToDecimalDigits(_ decimals:Int) -> Double
    {
        let a : Double = self
        let format : NumberFormatter = NumberFormatter()
        format.numberStyle = NumberFormatter.Style.decimal
        format.roundingMode = NumberFormatter.RoundingMode.halfUp
        format.maximumFractionDigits = 2
        let string: NSString = format.string(from: NSNumber(value: a as Double))! as NSString
        return string.doubleValue
    }
}
