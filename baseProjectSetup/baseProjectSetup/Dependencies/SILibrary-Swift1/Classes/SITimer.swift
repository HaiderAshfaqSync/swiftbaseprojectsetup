//
//  SITimer.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 14/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

class SITimer : NSObject {
    
    // MARK: Variables/Constants
    
    var timer:Timer?
    var timerCallbackBlock : ((_ sender:SITimer) -> Void)?
    
    // MARK: Lifecycle
    
    deinit {
        DebugLog("DEALLOC")
    }
    
    // MARK: Public
    
    func startTimer(_ duration:TimeInterval, repeats:Bool, userInfo: Any? = nil, timerCallbackBlock: @escaping ((_ sender:SITimer) -> Void)) {
        self.timerCallbackBlock = timerCallbackBlock
        timer = Timer.scheduledTimer(timeInterval: duration, target: self, selector: #selector(timerCallback), userInfo: userInfo, repeats: repeats)
        
        RunLoop.main.add(timer!, forMode: RunLoopMode.commonModes)
    }
    
    func stopTimer() {
        if let tmr = timer {
            tmr.invalidate()
        }
        
        timer = nil
        timerCallbackBlock = nil
    }
    
    @objc fileprivate func timerCallback() {
        if let block = timerCallbackBlock {
            block(self)
        }
    }
    
}
