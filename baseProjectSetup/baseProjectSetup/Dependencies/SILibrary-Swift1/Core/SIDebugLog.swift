//
//  SIDebugLog.swift
//  MozaicCollectionViewTest
//
//  Created by Haider Ashfaq on 04/12/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation

func DebugLog(_ items: Any..., filePath: String = #file, line: Int = #line, function: String = #function) {
    #if DEBUG
        var threadName = ""
        threadName = Thread.current.isMainThread ? "MAIN THREAD" : (Thread.current.name ?? "UNKNOWN THREAD")
        threadName = "[" + threadName + "] "
        
        let fileName = NSURL(fileURLWithPath: filePath).deletingPathExtension?.lastPathComponent ?? "???"
        
        let msg = " - \(items)"
        
        NSLog("-- " + threadName + fileName + "(\(line))" + " -> " + function + msg)
    #endif
}

func SilentLog(_ items: Any...) {
    #if DEBUG
        Swift.debugPrint(items)
    #endif
}

func SimulatorLog(_ items: Any...) {
    if SI.PLATFORM.IS_SIMULATOR {
        print(items)
    }
}

func SimulatorDebugLog(_ items: Any..., filePath: String = #file, line: Int = #line, function: String = #function) {
    if SI.PLATFORM.IS_SIMULATOR {
        DebugLog(items, filePath: filePath, line: line, function: function)
    }
}
