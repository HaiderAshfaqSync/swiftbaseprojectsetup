//
//  SIMacros.swift
//  MozaicCollectionViewTest
//
//  Created by Haider Ashfaq on 29/11/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit

struct Localised {
    static func key(_ key:String) -> String {
        return SILocalisation.getLocalisedValueForKey(key)
    }
}

struct SI
{
    
    struct APP
    {
        //static let DELEGATE:SIAppDelegate = UIApplication.shared.delegate as! SIAppDelegate
        
        static var DISPLAY_NAME: String? {
            if let bundleDisplayName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String {
                return bundleDisplayName
            } else if let bundleName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String {
                return bundleName
            }
            
            return nil
        }
        
        static var VERSION: String? {
            return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        }
        
        static var BUILD: String? {
            return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
        }
        
        static var BUNDLE_IDENTIFIER: String? {
            return Bundle.main.bundleIdentifier
        }
        
//        static var IS_SYNC_BUNDLE_IDENTIFIER:Bool {
//            if let identifier = BUNDLE_IDENTIFIER {
//                if identifier.containsString("syncinteractive") {
//                    return true
//                }
//            }
//            return false
//        }
        
        static var VERSION_AND_BUILD: String? {
            if VERSION != nil && BUILD != nil {
                if VERSION == BUILD {
                    return "v\(VERSION!)"
                } else {
                    return "v\(VERSION!)(\(BUILD!))"
                }
            }
            return nil
        }
        
        static var UUID: String? {
            // Changes on simulator each run BUT stays the same on a device
            // If the app is uninstalled, then reinstalled, it will change
            return UIDevice.current.identifierForVendor!.uuidString
        }
    }
    
    struct PLATFORM
    {
        static var IS_SIMULATOR: Bool {
            return TARGET_OS_SIMULATOR != 0
        }
        
        static var IS_IPAD: Bool {
            return UIDevice.current.userInterfaceIdiom == .pad
        }
        
        static var IS_IPHONE: Bool {
            return UIDevice.current.userInterfaceIdiom == .phone
        }
        
        static var IS_TV: Bool {
            if #available(iOS 9.0, *) {
                return UIDevice.current.userInterfaceIdiom == .tv
            } else {
                return false
            }
        }
        
        static var IS_CARPLAY: Bool {
            if #available(iOS 9.0, *) {
                return UIDevice.current.userInterfaceIdiom == .carPlay
            } else {
                return false
            }
        }
        
        static var IS_UNSPECIFIED: Bool {
            return UIDevice.current.userInterfaceIdiom == .unspecified
        }
    }
    
    struct SCREEN_SIZE
    {
        static let SCREEN_SCALE         = UIScreen.main.scale
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(SCREEN_WIDTH, SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(SCREEN_WIDTH, SCREEN_HEIGHT)
        //static let NAV_BAR_HEIGHT       = APP.DELEGATE.navigationController!.navigationBar.frame.height
        static let STATUS_BAR_HEIGHT    = UIApplication.shared.statusBarFrame.size.height
    }
    
    struct VERSION
    {
        static let SYS_VERSION_STRING   = (UIDevice.current.systemVersion as NSString)
        static let SYS_VERSION_FLOAT    = SYS_VERSION_STRING.floatValue
        
        static func SYSTEM_VERSION_EQUAL_TO(_ version: String) -> Bool {
            return UIDevice.current.systemVersion.compare(version,
                                                          options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
        }
        
        static func SYSTEM_VERSION_GREATER_THAN(_ version: String) -> Bool {
            return UIDevice.current.systemVersion.compare(version,
                                                          options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending
        }
        
        static func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_ version: String) -> Bool {
            return UIDevice.current.systemVersion.compare(version,
                                                          options: NSString.CompareOptions.numeric) != ComparisonResult.orderedAscending
        }
        
        static func SYSTEM_VERSION_LESS_THAN(_ version: String) -> Bool {
            return UIDevice.current.systemVersion.compare(version,
                                                          options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
        }
        
        static func SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(_ version: String) -> Bool {
            return UIDevice.current.systemVersion.compare(version,
                                                          options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending
        }
    }
}


