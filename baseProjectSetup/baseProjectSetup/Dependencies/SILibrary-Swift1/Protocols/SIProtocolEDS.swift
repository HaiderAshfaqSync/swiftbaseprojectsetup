//
//  SIProtocolEDS.swift
//  MozaicCollectionViewTest
//
//  Created by Haider Ashfaq on 29/11/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit

public protocol SIProtocolEDS : NSObjectProtocol {
    func titleForEmptyDataSet() -> String
    func titleFontForEmptyDataSet() -> UIFont
    func titleColourForEmptyDataSet() -> UIColor
    func descriptionForEmptyDataSet() -> String
    func descriptionFontForEmptyDataSet() -> UIFont
    func descriptionColourForEmptyDataSet() -> UIColor
    func customViewForEmptyDataSet() -> UIView?
}
