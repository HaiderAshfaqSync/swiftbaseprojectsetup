//
//  SIGradientView.swift
//  Serve
//
//  Created by Dan Hillman on 30/11/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

class SIGradientView: UIView {
    
    /*
     Usage:
    let fadedView = SIGradientView.newAutoLayout()
    fadedView.colors = [
        UIColor.blackColor().colorWithAlphaComponent(0.0),
        self.view.backgroundColor!
    ]
    */
    
    // MARK: - Class methods
    
    override class var layerClass : AnyClass {
        return CAGradientLayer.self
    }
    
    // MARK: - Public properties
    
    var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    var colors: [UIColor]? {
        get {
            guard let colors = gradientLayer.colors as? [CGColor] else { return nil }
            return colors.map { UIColor(cgColor:$0) }
        }
        set {
            gradientLayer.colors = newValue?.map { $0.cgColor }
        }
    }
    var locations: [Float]? {
        get {
            return gradientLayer.locations as? [Float]
        }
        set {
            gradientLayer.locations = newValue as [NSNumber]?
        }
    }
    var endPoint: CGPoint {
        get {
            return gradientLayer.endPoint
        }
        set {
            gradientLayer.endPoint = newValue
        }
    }
    var startPoint: CGPoint {
        get {
            return gradientLayer.startPoint
        }
        set {
            gradientLayer.startPoint = newValue
        }
    }
}
