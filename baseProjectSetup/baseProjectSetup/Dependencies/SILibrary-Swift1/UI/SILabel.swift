//
//  SILabel.swift
//  MozaicCollectionViewTest
//
//  Created by Haider Ashfaq on 04/12/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit

enum SITextPosition {
    case `default`
    case top
    case bottom
}

class SILabel : UILabel {
    
    var edgeInsets:UIEdgeInsets = UIEdgeInsets.zero
    var textPosition:SITextPosition = .default
    
    deinit {
        DebugLog("DEALLOC")
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        
        self.accessibilityTraits = UIAccessibilityTraitStaticText
        if #available(iOS 10.0, *) {
            self.adjustsFontForContentSizeCategory = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    convenience init(_ frame:CGRect = CGRect.zero,
                     edgeInsets: UIEdgeInsets = UIEdgeInsets.zero,
                     alignment:SITextPosition = .default) {
        self.init(frame:frame)
        
        self.edgeInsets = edgeInsets
        self.textPosition = alignment
    }
    
    override func drawText(in rect: CGRect) {
        var rectVar = rect
        if let text = self.text {
            if (text.characters.count > 0 && textPosition == .top) {
                let height:CGFloat = self.sizeThatFits(rectVar.size).height
                rectVar.origin.y = 0
                rectVar.size.height = height
                super.drawText(in: UIEdgeInsetsInsetRect(rectVar, edgeInsets))
                
            } else if (text.characters.count > 0 && textPosition == .bottom) {
                let height:CGFloat = self.sizeThatFits(rectVar.size).height
                rectVar.origin.y += rectVar.size.height - height
                rectVar.size.height = height
                super.drawText(in: UIEdgeInsetsInsetRect(rectVar, edgeInsets))
                
            } else {
                super.drawText(in: UIEdgeInsetsInsetRect(rectVar, edgeInsets))
            }
        } else {
            super.drawText(in: UIEdgeInsetsInsetRect(rectVar, edgeInsets))
        }
        
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insets = self.edgeInsets
        var rect = super.textRect(forBounds: UIEdgeInsetsInsetRect(bounds, insets), limitedToNumberOfLines: numberOfLines)
        rect.origin.x    -= insets.left
        rect.origin.y    -= insets.top
        rect.size.width  += (insets.left + insets.right)
        rect.size.height += (insets.top + insets.bottom)
        return rect
    }
    
}
