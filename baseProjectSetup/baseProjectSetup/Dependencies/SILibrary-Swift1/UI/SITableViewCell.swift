//
//  SITableViewCell.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 07/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit
import MGSwipeTableCell

class SITableViewCell: MGSwipeTableCell {
    
    // MARK: - Cell states
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if (self.isEditing) {
            
        } else {
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    // MARK: - Cell reuse
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
