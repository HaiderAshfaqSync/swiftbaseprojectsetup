//
//  SICollectionViewCell.swift
//  MozaicCollectionViewTest
//
//  Created by Haider Ashfaq on 29/11/2017.
//  Copyright © 2017 Haider Ashfaq. All rights reserved.
//

import Foundation
import UIKit

class SICollectionViewCell: UICollectionViewCell {
    
    // MARK: - Lifecycle
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    // MARK: - Cell reuse
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
