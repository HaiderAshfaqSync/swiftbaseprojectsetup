//
//  SITableView.swift
//  TalkingMotors
//
//  Created by Dan H (Sync Interactive) on 02/06/2016.
//  Copyright © 2016 Sync Interactive. All rights reserved.
//

import Foundation
import UIKit

class SITableView: UITableView {
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        return true
    }
}
